'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var CampaignSchema = new Schema({
  criteria: [{
    text: String,
    _id: false
  }],
  distance: Number,
  token: String,
  message: String,
  price: Number,
  gender: String,
  age: String,
  limit: {type: Number, default: 30},
  hoursLimit: {type: Number, default: 72},
  user: {type: Schema.ObjectId, ref: 'User'},
  createdAt: {type: Date, default: function(){return new Date();}},
  available: {type: Number},
  userGroup: [{type: Schema.ObjectId, ref: 'User'}],
  userScanned: [{
    user: {type: Schema.ObjectId, ref: 'User'},
    scannedAt: {type: Date, default: function(){return new Date();}}
  }],
  thumbsUp: [{type: Schema.ObjectId, ref: 'User'}],
  thumbsDown: [{type: Schema.ObjectId, ref: 'User'}],
  userScannedCount: {type: Number, default: 0},
  geo: {
    type: [Number],
    index: '2d'
  },
  active: {type: Boolean, default: false},
  status: {type: String, default: 'moderate'}
});

/**
 * Pre-save hook
 */
CampaignSchema
  .pre('save', function (next) {
    if (!this.isNew) return next();

    this.constructor.count({
      user: this.user,
      active: true
    }, function (err, count) {
      if (err) {
        throw err;
      }
      if (count >= 1) {
        next(new Error('Invalid campaign create'));
      } else {
        next();
      }
    })
  });

module.exports = mongoose.model('Campaign', CampaignSchema);
