'use strict';

var express = require('express');
var controller = require('./campaign.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', controller.index);
router.get('/me/getNonModerated', auth.isAuthenticated(), controller.getNonModerated);
router.get('/me/getCriteriaList', auth.isAuthenticated(), controller.getCriteriaList);
router.get('/me/getRemainingTime/:id', auth.isAuthenticated(), controller.getRemainingTime);
router.get('/me/getActive', auth.isAuthenticated(), controller.getActive);
router.get('/:id/accept', auth.isAuthenticated(), controller.accept);
router.get('/:id/reject', auth.isAuthenticated(), controller.reject);
router.get('/:id', controller.show);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', auth.isAuthenticated(), controller.update);
router.put('/:id/scan', auth.isAuthenticated(), controller.scan);
router.put('/:id/reach', auth.isAuthenticated(), controller.reach);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);
router.put('/me/set-mark', auth.isAuthenticated(), controller.setMark);
router.put('/me/get-mark', auth.isAuthenticated(), controller.getMark);

module.exports = router;
