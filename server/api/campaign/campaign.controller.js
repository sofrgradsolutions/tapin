'use strict';

var _ = require('lodash');
var Campaign = require('./campaign.model');
var Business = require('../user/business.model');
var Category = require('./category.model');
var Criteria = require('./criteria.model');
var User = require('../user/user.model');
var UserLocation = require('../user/user-location.model');
var util = require('util');
//var mileToMi = 1609.34;
var mileToKm = 1.60934;
var geolib = require('geolib');
var Moniker = require('moniker');
var fs = require('fs');
var nodemailer = require('nodemailer');
var config = require('../../config/environment');
var distances = config.distances;


var findCampaignByToken = function (token, done) {

	Campaign.find({token: token, active: true}, function (err, campaigns) {
		if (err) {
			return handleError(res, err);
		}
		return done(campaigns.length === 0);
	});
};

var generateUniqToken = function (res, newCampaign) {
	var names = Moniker.generator([Moniker.noun]);
	var token = names.choose() + _.random(1, 20);
	var done = function (isUnique) {
		if (!isUnique) {
			token = names.choose() + _.random(1, 20);
			findCampaignByToken(token, done);
		} else {
			newCampaign.set('token', token);
			newCampaign.save(function (err, campaign) {
				if (err) {
					return handleError(res, err);
				}
				return res.json(201, campaign);
			});
		}
	};
	findCampaignByToken(token, done);
};


var diffCriteria = function (criteriaList1, criteriaList2, genderList, ageList, userGender, userAge) {
	var isInclude = false;
	genderList = genderList.split(",");
	ageList = ageList.split(",");

	if (_.indexOf(genderList, userGender) !== -1 && _.indexOf(ageList, userAge) !== -1) {

		if (!_.isArray(criteriaList2) || criteriaList2.length === 0) {
			isInclude = true;
		} else {
			_.each(criteriaList1, function (item1) {
				_.each(criteriaList2, function (item2) {
					if (item1.text.toLowerCase() == item2.text.toLowerCase()) {
						isInclude = true;
					}
				});
			});
		}
	}
	return isInclude;
};

var sendAPNsMessage = function (iphoneTokens, message, id) {
	console.log(iphoneTokens);
	console.log('start sent');
	if (iphoneTokens.length === 0) {
		return;
	}
	var certData = fs.readFileSync("server/config/cert.pem");
	var keyData = fs.readFileSync("server/config/key.pem");
	var objectCert = {
		certData: certData,
		keyData: keyData/*,
		 passphrase : "1234"*/
	};
	var sender = new (require('nodejs-apns-secure')).SenderApns(objectCert, true);

	var apnsMessages = [];
	for (var i = 0, n = iphoneTokens.length; i < n; i++) {
		apnsMessages.push({
			_id: "m" + (new Date()).getTime() + '_' + i,
			expiry: 0,
			payload: {
				aps: {
					alert: message,
					sound: "default"
				},
				business: id
			}
		});
	}

	sender.sendThroughApns(apnsMessages, iphoneTokens,
			function (resultStatusArray) {
				console.log(resultStatusArray);
			},
			function (error) {
				console.log(error);
			}
	);
};

// Get list of campaigns
exports.index = function (req, res) {
	Campaign.find(function (err, campaigns) {
		if (err) {
			return handleError(res, err);
		}
		return res.json(200, campaigns);
	});
};

// Get list of non moderated campaigns
exports.getNonModerated = function (req, res) {
	var result = [];

	Campaign.find({status: 'moderate'}, function (err, campaigns) {
		if (err) {
			return handleError(res, err);
		}

		var mappedCampaigns = _.map(campaigns, function (item) {
			return item.user;
		});

		Business.find({user: {$in: mappedCampaigns}}, function (err, businesses) {
			if (err) {
				return handleError(res, err);
			}

			_.forEach(campaigns, function (camp) {
				_.forEach(businesses, function (bus) {
					if (camp.user.toString() == bus.user.toString()) {
						result.push({
							campaign: camp,
							business: bus
						});
					}
				})
			});

			return res.json(200, result);
		});
	})
};

exports.getRemainingTime = function (req, res) {
	Campaign.findById(req.params.id, function (err, campaign) {
		if (err) {
			return handleError(res, err);
		}
		if (!campaign) {
			return res.send(404);
		}
        if (!campaign.active) {
            return res.json(200, {
                remainingHours: 0,
                remainingMinutes: 0,
                remainingSeconds: 0
            })
        } else {
            var timeExpired = new Date() - campaign.createdAt,
                remainingMilliseconds = campaign.hoursLimit * 60 * 60 * 1000 - timeExpired,
                remainingHours, remainingMinutes, remainingSeconds, x;

            if (remainingMilliseconds < 0) {
                remainingHours = 0;
                remainingMinutes = 0;
                remainingSeconds = 0;
            } else {
                x = remainingMilliseconds / 1000;
                remainingSeconds = Math.floor(x % 60);
                x /= 60;
                remainingMinutes = Math.floor(x % 60);
                x /= 60;
                remainingHours = Math.floor(x % 24);
            }

            return res.json(200, {
                remainingHours: remainingHours,
                remainingMinutes: remainingMinutes,
                remainingSeconds: remainingSeconds
            })
        }
	});
};

exports.getActive = function (req, res) {
    req.checkQuery('lat', 'Invalid latitude').notEmpty().isFloat();
    req.checkQuery('long', 'Invalid longitude').notEmpty().isFloat();
    var errors = req.validationErrors();
    if (errors) {
        return res.send('There have been validation errors: ' + util.inspect(errors), 400);
    }
    var coords = [];
    coords[0] = parseFloat(req.param('long'));
    coords[1] = parseFloat(req.param('lat'));
    var result = [];

    Campaign.find({active: true}, function (err, campaigns) {
        if (err) {
            return handleError(res, err);
        }

        var mappedCampaigns = _.map(campaigns, function (item) {
            return item.user;
        });

		Business.find({user: {$in: mappedCampaigns}}, function (err, businesses) {
            if (err) {
                return handleError(res, err);
            }

            _.forEach(campaigns, function (camp) {
                _.forEach(businesses, function (bus) {
                    if (camp.user.toString() == bus.user.toString()) {
						var timeExpired = new Date() - camp.createdAt,
							remainingMilliseconds = camp.hoursLimit * 60 * 60 * 1000 - timeExpired,
							remainingHours, remainingMinutes, remainingSeconds, x;

						if (remainingMilliseconds < 0) {
							remainingHours = 0;
							remainingMinutes = 0;
							remainingSeconds = 0;
						} else {
							x = remainingMilliseconds / 1000;
							remainingSeconds = Math.floor(x % 60);
							x /= 60;
							remainingMinutes = Math.floor(x % 60);
							x /= 60;
							remainingHours = Math.floor(x % 24);
						}

						var remainingTime = {
							remainingHours: remainingHours,
							remainingMinutes: remainingMinutes,
							remainingSeconds: remainingSeconds
						};

                        result.push({
                            campaign: camp,
                            business: bus,
                            distance: geolib.getDistance([coords[1], coords[0]], [bus.geo[1], bus.geo[0]]),
							remainingTime: remainingTime
                        });
                    }
                })
            });

            return res.json(200, result);
        });
    })
};

exports.getCriteriaList = function (req, res) {

  Criteria.find().populate('category').exec(function (err, criteria) {
    if (err) {
      return handleError(res, err);
    }
    if (!criteria) {
      return res.send(404, {});
    }

    return res.json(200, criteria);
  });
};

exports.accept = function (req, res) {
	var hosts = req.headers.host.split(':'),
			message = 'Hello, <br /><br />your campaign has been successfully accepted by moderator.',
			acceptMessage = 'Your campaign has been successfully accepted';

	Campaign.findOne({
		'_id': req.params.id
	}
	).populate('user').exec(function (err, campaign) {
		if (err) {
			return handleError(res, err);
		}
		if (!campaign) {
			return res.send(404);
		}
		campaign.status = 'accept';
		campaign.active = true;

		campaign.save(function (err, campaign) {
			if (err) {
				return handleError(res, err);
			}

			UserLocation.find({
				geo: {
					$near: campaign.geo,
					$maxDistance: campaign.distance * mileToKm / 111.12
				}
			}).populate({
				path: 'user',
				match: {
					role: 'user'
				}
			}).exec(function (err, users) {
				if (err) {
					console.log(err);
					return handleError(res, err);
				}

				campaign.userGroup = [];
				var deviceTokens = [];
				_.forEach(users, function (user) {
					//console.log(geolib.getDistance(user.geo, campaign.geo));
					if (user.user && diffCriteria(campaign.criteria, user.user.criteria, campaign.gender, campaign.age, user.user.gender, user.user.age)) {
						if (user.user.deviceToken) {
							deviceTokens.push(user.user.deviceToken);
						}
						campaign.userGroup.push(user.user._id);
						/*
						 _.forEach(user.user.socketId, function (socketId) {
						 global.io.sockets.socket(socketId).emit(campaign.message);
						 });*/
					}
				});
				Business.findOne({user: campaign.user}, function (err, business) {
					if (err) {
						return handleError(res, err);
					}

					sendAPNsMessage(deviceTokens, campaign.message, business._id);
					campaign.save(function (err) {
						if (err) {
							return handleError(res, err);
						}

						var transporter = nodemailer.createTransport({
							service: 'Gmail',
							auth: {
								user: config.smpt.login,
								pass: config.smpt.pass
							}
						});
						transporter.sendMail({
							from: 'noreply@' + hosts[0],
							to: campaign.user.email,
							subject: 'Your campaign has been successfully accepted',
							html: message
						}, function (error, info) {
							if (error) {
								console.log(error);
							} else {
								console.log(info);
							}
						});

						// emit accept to socket.
						_.forEach(campaign.user.socketId, function (socketId) {
							global.io.sockets.socket(socketId).emit('campaign:accept', {message: acceptMessage});
						});

						return res.json({ok: 'ok'});
					});
				});
			});
		});
	});
};

exports.reject = function (req, res) {
	var hosts = req.headers.host.split(':'),
			message = 'Hello, <br /><br />your campaign has been rejected by moderator. Please, edit the campaign information and resubmit.',
			rejectMessage = 'Your campaign has been rejected by moderator';

	Campaign.findOne({
		'_id': req.params.id
	}
	).populate('user').exec(function (err, campaign) {
		if (err) {
			return handleError(res, err);
		}
		if (!campaign) {
			return res.send(404);
		}
		campaign.status = 'reject';

		campaign.save(function (err) {
			if (err) {
				return handleError(res, err);
			}

			var transporter = nodemailer.createTransport({
				service: 'Gmail',
				auth: {
					user: config.smpt.login,
					pass: config.smpt.pass
				}
			});
			transporter.sendMail({
				from: 'noreply@' + hosts[0],
				to: campaign.user.email,
				subject: 'Your campaign has been rejected by moderator',
				html: message
			}, function (error, info) {
				if (error) {
					console.log(error);
				} else {
					console.log(info);
				}
			});

			// emit reject to socket.
			_.forEach(campaign.user.socketId, function (socketId) {
				global.io.sockets.socket(socketId).emit('campaign:reject', {message: rejectMessage});
			});

			return res.json({ok: 'ok'});
		});
	})
};

// Get a single campaign
exports.show = function (req, res) {
	Campaign.findById(req.params.id, function (err, campaign) {
		if (err) {
			return handleError(res, err);
		}
		if (!campaign) {
			return res.send(404);
		}
		return res.json(campaign);
	});
};

// Creates a new campaign in the DB.
exports.create = function (req, res) {
	//sendAPNsMessage('fc44556987000e1a71c9b57db3b3e5e6a59889ccfdaa7ab3f40315c20833d052', 'Test Message');
	// parameter validation
	req.checkBody('criteria', 'Invalid criteria').notEmpty().isArray();
	req.checkBody('price', 'Invalid price').notEmpty().isFloat();
	req.checkBody('distance', 'Invalid distance').notEmpty().isFloat();
	req.checkBody('message', 'Invalid message').notEmpty();

	var errors = req.validationErrors();
	if (errors) {
		return res.send('There have been validation errors: ' + util.inspect(errors), 400);
	}

	var userId = req.user._id;

	Business.findOne({user: userId}, function (err, business) {
		if (err) {
			return handleError(res, err);
		}

		// create new campaign
		var newCampaign = new Campaign(req.body);
		newCampaign.user = userId;
		newCampaign.geo = business.geo;
		newCampaign.save(function (err) {
			if (err) {
				return handleError(res, err);
			}
		   generateUniqToken(res, newCampaign);
		});
	});
};

// Updates an existing campaign in the DB.
exports.update = function (req, res) {
	if (req.body._id) {
		delete req.body._id;
	}
	Campaign.findById(req.params.id, function (err, campaign) {
		if (err) {
			return handleError(res, err);
		}
		if (!campaign) {
			return res.send(404);
		}
		var updated = _.merge(campaign, req.body);
		updated.save(function (err) {
			if (err) {
				return handleError(res, err);
			}
			return res.json(200, campaign);
		});
	});
};

// update user group after scanning qr code.
exports.scan = function (req, res) {
	var userId = req.user._id;

	Campaign.findOne({
		'token': req.params.id,
		'active': true,
		'userScanned.user': {$ne: userId}
	}).populate('user').exec(function (err, campaign) {
		if (err) {
			return handleError(res, err);
		}
		if (!campaign) {
			return res.send(404, {});
		}
		//if(!_.find(campaign.userScanned, userId)) {
		//  campaign.userScanned.push(userId);
		//}

		campaign.userScanned.push({
			user: userId,
			scannedAt: Date.now()
		});
		campaign.userScannedCount = campaign.userScannedCount + 1;

		if (campaign.userScannedCount == campaign.limit) {
			campaign.set('active', false);
		}

		campaign.save(function (err) {
			if (err) {
				return handleError(res, err);
			}

			// emit update to socket.
			_.forEach(campaign.user.socketId, function (socketId) {
				global.io.sockets.socket(socketId).emit('campaign:update', campaign);
			});

			return res.send(200, campaign);
		});
	});
};

// Increase reached in campaign.
exports.reach = function (req, res) {
	var userId = req.user._id;

	User.findById(userId, function (err, user) {
		if (err) {
			return handleError(res, err);
		}

		if (!user) {
			return res.send(404);
		}

		Campaign.findOne({
			'_id': req.params.id,
			'userGroup': {$ne: userId},
			'active': true
		}).populate('user').exec(function (err, campaign) {
			if (err) {
				return handleError(res, err);
			}
			if (!campaign) {
				return res.send(404, {});
			}
			campaign.userGroup.push(userId);
			campaign.save(function (err) {
				if (err) {
					return handleError(res, err);
				}

				// emit update to socket.
				_.forEach(campaign.user.socketId, function (socketId) {
					global.io.sockets.socket(socketId).emit('campaign:update', campaign);
				});

				Business.findOne({user: campaign.user}, function (err, business) {
					if (err) {
						return handleError(res, err);
					}

					sendAPNsMessage([user.deviceToken], campaign.message, business._id);
					/*
					 _.forEach(user.socketId, function (socketId) {
					 //console.log(' [x] sending push notification to end user : found new campaign [%s]', campaign.message);
					 global.io.sockets.socket(socketId).emit(campaign.message);
					 });*/

					return res.send(200, {});
				});
			});
		});
	});
};

// Deletes a campaign from the DB.
exports.destroy = function (req, res) {
	Campaign.findById(req.params.id, function (err, campaign) {
		if (err) {
			return handleError(res, err);
		}
		if (!campaign) {
			return res.send(404);
		}
		campaign.remove(function (err) {
			if (err) {
				return handleError(res, err);
			}
			return res.send(204);
		});
	});
};

exports.setMark = function(req, res) {
  var userId = req.user._id;
  var thumbs = req.param('thumbs');

  Campaign.findById(req.param('campaign'), function (err, campaign) {
    if (err) {
      return handleError(res, err);
    }
    if (!campaign) {
      return res.send(404);
    }


    var indexUp = campaign.thumbsUp.indexOf(userId);
    var indexDown = campaign.thumbsDown.indexOf(userId);


    if (indexDown > -1 && thumbs == 'up') {
      campaign.thumbsDown.splice(indexDown, 1);
    }
    if (indexUp > -1 && thumbs == 'down') {
      campaign.thumbsUp.splice(indexUp, 1);
    }
    if (indexUp === -1 && thumbs == 'up') {
      campaign.thumbsUp.push(userId);
    }
    if (indexDown === -1 && thumbs == 'down') {
      campaign.thumbsDown.push(userId);
    }

    campaign.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.send(200, {ok: 'ok'});
    });
  });
};

exports.getMark = function(req, res) {
  var userId = req.user._id;
  var thumbs = null;

  Campaign.findById(req.param('campaign'), function (err, campaign) {
    if (err) {
      return handleError(res, err);
    }
    if (!campaign) {
      return res.send(404);
    }


    var indexUp = campaign.thumbsUp.indexOf(userId);
    var indexDown = campaign.thumbsDown.indexOf(userId);

    if (indexDown > -1) {
      thumbs = 'down';
    }
    if (indexUp > -1) {
      thumbs = 'up';
    }

    return res.send(200, {thumbs: thumbs});

  });
};

function handleError(res, err) {
	return res.send(500, err);
}
