'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var CriteriaSchema = new Schema({
  name: String,
  category: {type: Schema.ObjectId, ref: 'Category'}
});

module.exports = mongoose.model('Criteria', CriteriaSchema);
