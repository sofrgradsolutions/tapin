'use strict';

var User = require('./user.model');
var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var _ = require('lodash');
var async = require('async');
var distances = config.distances;
var util = require('util');
var UserLocation = require('./user-location.model');
var Business = require('./business.model');
var DailyDeal = require('../business/dailydeal.model');
var Campaign = require('../campaign/campaign.model');
var Criteria = require('../campaign/criteria.model');
var geolib = require('geolib');
var mileToMi = 1609.34;
var mileToKm = 1.60934;
var radianToMile = 3959;
var stripe = require("stripe")(
		config.stripe
		);
var nodemailer = require('nodemailer');
var fs = require('fs');
var Moniker = require('moniker');
var moment = require('moment');

var validationError = function (res, err) {
	return res.json(422, err);
};

var diffCriteria = function (criteriaList1, criteriaList2) {
	var isInclude = false;

	if (!_.isArray(criteriaList2) || criteriaList2.length === 0) {
		isInclude = true;
	} else {
		_.each(criteriaList1, function (item1) {
			_.each(criteriaList2, function (item2) {
				if (item1.text.toLowerCase() == item2.text.toLowerCase()) {
					isInclude = true;
				}
			});
		});
	}
	return isInclude;
};

var findBusinessByToken = function (token, done, callback) {
	Business.find({token: token}, function (err, bussiness) {
		if (err) {
			return callback(err);
		}
		return done(bussiness.length === 0);
	});
};


var generateUniqToken = function (business, callback) {
	var names = Moniker.generator([Moniker.noun]);
	var token = names.choose() + _.random(1, 20);
	var done = function (isUniq) {
		if (!isUniq) {
			token = names.choose() + _.random(1, 20);
			findCampaignByToken(token, done);
		} else {
			business.set('token', token);
			business.save(function (err, b) {
				if (err) {
					return callback(err);
				}
				return callback();
			});
		}
	};
	findBusinessByToken(token, done, callback);
};

exports.autoCompleteCampaign = function() {
	console.log('autoCompleteCampaign');
	var today = new Date();

	Campaign.find({
		active: true
	}, function (err, campaigns) {
		_.each(campaigns, function (campaign) {
			var dateCheck = new Date(today.getFullYear(), today.getMonth(), today.getDate(), today.getHours() - parseInt(campaign.hoursLimit), today.getMinutes());

			if (campaign.createdAt < dateCheck) {
				campaign.set('active', false);
				campaign.save(function (err) {
					if (err) {
						console.log(err);
						return handleError(res, err);
					}
				});
			}
		});
	});
};


/**
 * Request restore password email
 *
 */
exports.recoverPasswordStep1 = function (req, res) {
	console.log(req.headers);
	var hosts = req.headers.host.split(':');

	var recoverToken = 'pr' + (new Date()).getTime();
	var recoverLink = 'http://' + req.headers.host + '/recover-password?stage=3&token=' + recoverToken;
	//console.log(recoverLink);
	User.find({email: req.body.email}, '-salt -hashedPassword', function (err, users) {
		if (err) {
			return res.send(500, err);
		}

		if (users.length === 0) {
			res.send(500, {message: 'User with email "' + req.body.email + '" don\'t found in database.'});
		} else {
			var user = users[0];

			user.recoverToken = recoverToken;
			user.save(function (err) {
				if (err) {
					return validationError(res, err);
				}
				var message = 'Hello, <br /><br />we received a request for password change for user ' + user.email +
						'. <a href="' + recoverLink + '">Go to this page</a> to set your new password.' +
						'<br /><br />If you don\'t request new password, please do not respond on this email.';
				var transporter = nodemailer.createTransport({
					service: 'Gmail',
					auth: {
						user: config.smpt.login,
						pass: config.smpt.pass
					}
				});
				transporter.sendMail({
					from: 'noreply@' + hosts[0],
					to: user.email,
					subject: 'Forgot Your TapIn Password',
					html: message
				}, function (error, info) {
					if (error) {
						console.log(error);
					} else {
						console.log(info);
					}
				});
				res.json(200, {ok: true});
			});
		}
	});
};


/**
 * Request change email letter
 *
 */

exports.changeEmailStep1 = function (req, res) {
	var hosts = req.headers.host.split(':');

	var emailChangeToken = 'che' + (new Date()).getTime();
	var recoverLink = 'http://' + req.headers.host + '/set-new-email?token=' + emailChangeToken;
	console.log(recoverLink);
	var userId = req.user._id;

	User.findById(userId, function (err, user) {

		if (err) {
			return res.send(500, err);
		}
		User.find({email: req.body.email}, '-salt -hashedPassword', function (err, users) {
			console.log(user);
			if (users.length > 0) {
				res.send(400, {message: 'User with email "' + req.body.email + '"  already exists.'});
			} else {

				user.emailChangeToken = emailChangeToken;
				user.newEmail = req.body.email;

				user.save(function (err) {
					if (err) {
						return validationError(res, err);
					}
					var message = 'Hello, <br /><br />we received a request for email change for user ' + user.email +
							'. <a href="' + recoverLink + '">Go to this page</a> to set your new email.' +
							'<br /><br />If you don\'t request new email, please do not respond on this email.';
					var transporter = nodemailer.createTransport({
						service: 'Gmail',
						auth: {
							user: config.smpt.login,
							pass: config.smpt.pass
						}
					});
					transporter.sendMail({
						from: 'noreply@' + hosts[0],
						to: user.email,
						subject: 'Change Your TapIn Email',
						html: message
					}, function (error, info) {
						if (error) {
							console.log(error);
						} else {
							console.log(info);
						}
					});
					res.json(200, {ok: true});
				});
			}
		});
	});
};


/**
 * Set new email (for current user ? )
 */
exports.changeEmailStep2 = function (req, res) {
	var token = req.body.token;
	User.find({emailChangeToken: token}, '-salt -hashedPassword', function (err, users) {
		if (err) {
			return res.send(500, err);
		}
		if (users.length === 0) {
			res.send(400, {message: 'Invalid change email token.'});
		} else {
			var user = users[0];
			user.email = user.newEmail;
			user.emailChangeToken = '';
			user.newEmail = '';
			user.save(function (err) {
				if (err) {
					return validationError(res, err);
				}
				res.json(200, {ok: true});
			});
		}
	});
};


/**
 * Set new password for recover password
 */
exports.recoverPasswordStep2 = function (req, res) {
	var recoverToken = req.body.recoverToken;
	console.log(recoverToken);
	User.find({recoverToken: recoverToken}, '-salt -hashedPassword', function (err, users) {
		if (err) {
			return res.send(500, err);
		}
		console.log(users);
		if (users.length === 0) {
			res.send(500, {message: 'Invalid recover password token.'});
		} else {
			var user = users[0];
			user.password = req.body.password;
			user.recoverToken = '';
			user.save(function (err) {
				if (err) {
					return validationError(res, err);
				}
				res.json(200, {ok: true});
			});
		}
	});
};

/**
 * Request confirm user email
 */
var confirmEmail = function (req, res, user) {
	var hosts = req.headers.host.split(':');

	var confirmationToken = 'pr' + (new Date()).getTime();
	var confirmationLink = 'http://' + req.headers.host + '/confirm-email?stage=2&token=' + confirmationToken;

	user.confirmationToken = confirmationToken;
	user.save(function (err) {
		if (err) {
			return validationError(res, err);
		}
		var message = 'Hello, <br /><br />you have been registered with username ' + user.username +
				'. <a href="' + confirmationLink + '">Go to this page</a> to confirm your registration email.' +
				'<br /><br />If you don\'t register in our service, please do not respond on this email.';
		var transporter = nodemailer.createTransport({
			service: 'Gmail',
			auth: {
				user: config.smpt.login,
				pass: config.smpt.pass
			}
		});
		transporter.sendMail({
			from: 'noreply@' + hosts[0],
			to: user.email,
			subject: 'Confirm Your TapIn Password',
			html: message
		}, function (error, info) {
			if (error) {
				console.log(error);
			} else {
				console.log(info);
			}
		});
		res.json(200, {ok: true});
	});

};

/**
 * Register new user by confirmation token.
 */
exports.checkConfirmEmail = function (req, res) {

	var confirmationToken = req.body.token;

	User.find({confirmationToken: confirmationToken}, '-salt -hashedPassword', function (err, users) {
		if (err) {
			return res.send(500, err);
		}
		console.log(users);
		if (users.length === 0) {
			res.send(500, {message: 'Invalid confirmation token.'});
		} else {
			var user = users[0];
			//user.confirmationToken = '';
			user.step = 'onboard';
			user.save(function (err) {
				if (err) {
					return validationError(res, err);
				}
				console.log(user._id);
				var token = jwt.sign({_id: user._id}, config.secrets.session, {expiresInMinutes: 60 * 5});

				res.json(200, {token: token, step: 'onboard'});
			});
		}
	});
};


/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function (req, res) {
	User.find({}, '-salt -hashedPassword', function (err, users) {
		if (err)
			return res.send(500, err);
		res.json(200, users);
	});
};

/**
 * Creates a new user
 */
exports.create = function (req, res, next) {
	console.log(' [x] request body : ', req.body, req.files);
	var newUser = new User(req.body);
	newUser.provider = 'local';
	if (req.files && req.files.avatar) {
		newUser.set('avatar', req.files.avatar);
	}
	if (req.body.role) {
		newUser.set('role', req.body.role);
	}

	Criteria.find({}, function (err, criteria) {
		if (err) {
			return handleError(res, err);
		}

		if (req.body.role === 'user') {
			newUser.criteria = _.map(criteria, function (elem) {
				return {text: elem.name}
			});
		}

		newUser.save(function (err, user) {
			if (err)
				return handleError(res, err);

			if (req.body.lat && req.body.long) {
				if (user.role === 'user') {

					var newUserLocation = new UserLocation();
					newUserLocation.set('user', user._id);
					newUserLocation.set('geo', [req.body.long, req.body.lat]);
					newUserLocation.save(function (err, location) {
						if (err)
							return handleError(res, err);
						var token = jwt.sign({_id: user._id}, config.secrets.session, {expiresInMinutes: 60 * 5});
						res.json({token: token});
					});

				} else if (user.role === 'manager') {

					var newBusiness = new Business(req.body);
					newBusiness.set('user', user._id);
					newBusiness.set('geo', [req.body.long, req.body.lat]);
					var oppts = [];
					_.forEach(distances, function (distance) {
						oppts.push({
							distance: distance,
							userGroup: []
						});
					});
					newBusiness.set('opportunity', oppts);
					newBusiness.save(function (err) {
						if (err) {
							console.log(err);
							return handleError(res, err);
						}
						//var token = jwt.sign({_id: user._id}, config.secrets.session, {expiresInMinutes: 60 * 5});

						//res.json({token: token});
						//res.json({success: true});
						confirmEmail(req, res, user);
					});
				}
			} else {
				if (user.role === 'manager') {
					var newBusiness = new Business(req.body);
					newBusiness.set('user', user._id);
					newBusiness.save(function (err) {
						if (err) {
							console.log(err);
							return handleError(res, err);
						}
						confirmEmail(req, res, user);
					});
				} else {
					var token = jwt.sign({_id: user._id}, config.secrets.session, {expiresInMinutes: 60 * 5});
					res.json({token: token});
				}
			}
		});
	});
};

/**
 * Get a single user
 */
exports.show = function (req, res, next) {
	var userId = req.params.id;

	User.findById(userId, function (err, user) {
		if (err)
			return next(err);

		if (!user)
			return res.send(401);
		res.json(user.profile);
	});

};

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function (req, res) {
	User.findByIdAndRemove(req.params.id, function (err, user) {
		if (err)
			return res.send(500, err);
		return res.send(204);
	});
};

/**
 * Change a users password
 */
exports.changePassword = function (req, res, next) {
	var userId = req.user._id;
	var oldPass = String(req.body.oldPassword);
	var newPass = String(req.body.newPassword);

	User.findById(userId, function (err, user) {
		if (user.authenticate(oldPass)) {
			user.password = newPass;
			user.save(function (err) {
				if (err)
					return validationError(res, err);
				res.send(200);
			});
		} else {
			res.send(403);
		}
	});
};

exports.sendSupport = function (req, res, next) {
	var userId = req.user._id;
	User.findById(userId, function (err, user) {
		var transporter = nodemailer.createTransport({
			service: 'Gmail',
			auth: {
				user: config.smpt.login,
				pass: config.smpt.pass
			}
		});
		//console.log(config.smpt);
		transporter.sendMail({
			from: user.email,
			to: 'yr.bogdan@gmail.com',
			subject: 'Support: TapIn',
			text: req.body.message
		}, function (error, info) {
			if (error) {
				console.log(error);
			} else {
				console.log(info);
			}
		});
		if (err) {
			return res.send(400, err);
		}
		return res.send(200, {});
	});
};


exports.checkIsPaid = function (req, res, next) {
	var userId = req.user._id;
	User.findById(userId, function (err, user) {
		var datePaid = new Date(user.get('datePaid'));
		var dayLeft = ((new Date()).getTime() - datePaid.getTime()) / (3600 * 24 * 1000);
		if (dayLeft > 30) {
			user.set('isPaid', false);
			user.save(function (err) {
				if (err) {
					return res.send(400, err);
				}
				return res.send(200, {isPaid: false});
			});
		} else {
			return res.send(200, {isPaid: user.get('isPaid')});
		}
	});
};

exports.checkout = function (req, res, next) {
	var userId = req.user._id;
	var amount = Math.ceil(req.body.amount * 100);
	if (req.body.user) {
		userId = req.body.user;
	}

	Business.findOne({user: userId}, function (err, business) {
		stripe.charges.create({
			amount: amount,
			currency: "usd",
			card: {
				number: business.cardnumber,
				exp_month: business.expMonth,
				exp_year: business.expYear,
				cvc: business.CVC
			},
			description: "Charge for TapIn create campaign"
		}, function (err, charge) {
			if (err) {
				return res.send(400, {errors: err});
			} else {
				async.series([
					function (callback) {
						/*
						 User.findById(userId, function (err, user) {
						 user.set('isPaid', true);
						 user.set('step', 'finish');
						 user.set('datePaid', Date.now());
						 user.save(function (err) {
						 if (err)
						 return callback(err);
						 callback();
						 });
						 });*/
						callback();
					}
				], function (err) {
					if (err) {
						return next(err);
					}
					return res.send(200, {});
				});
			}
		});
	});
};

exports.update = function (req, res, next) {
	var userId = req.user._id;
	var oldPass = String(req.body.oldPassword);
	var newPass = String(req.body.newPassword);

	var role;

	async.series([
		function (callback) {
			User.findById(userId, function (err, user) {
				role = user.role;
				if (!newPass) {
					if (user.authenticate(oldPass)) {
						user.password = newPass;
					} else {
						return callback(403);
					}
				}
				user.set('criteria', req.body.criteria);
				if (req.body.step) {
					user.set('step', req.body.step);
				}
				user.save(function (err) {
					if (err)
						return callback(err);
					callback();
				});
			});
		},
		function (callback) {
			if (role == 'manager') {
				Business.findOne({user: userId}, function (err, business) {
					if (err) {
						return callback(err);
					}
					if (!business) {
						return callback(404);
					}
					delete req.body.business.opportunity;
					business = _.merge(business, req.body.business);
					//business.set('businessCategory', req.body.businessCategory.toArray(), {strict: false});
					business.set('businessCategory', undefined);
					business.businessCategory = [];
					req.body.businessCategory.forEach(function (category) {
						business.businessCategory.push(category);
					});

					if (req.body.business.description) {
						business.set('description', undefined);
						business.description = [];
						req.body.business.description.forEach(function (item) {
							business.description.push(item);
						});
					}

					if (req.body.lat && req.body.long) {
						business.set('geo', [parseFloat(req.body.long), parseFloat(req.body.lat)]);
					}

					business.save(function (err, b) {
						if (err) {
							return callback(err);
						}
						if (!b.token) {
							generateUniqToken(b, callback);
						} else {
							callback();
						}
					});
				});
			} else {
				callback();
			}
		}
	], function (err) {
		if (err) {
			return next(err);
		}
		return res.send(200, {});
	});
};

/**
 * Update user location
 * @param req
 * @param res
 * @param next
 */
exports.updateLocation = function (req, res, next) {
	// parameter validation
	req.checkBody('lat', 'Invalid latitude').notEmpty().isFloat();
	req.checkBody('long', 'Invalid longitude').notEmpty().isFloat();

	var errors = req.validationErrors();
	if (errors) {
		return res.send('There have been validation errors: ' + util.inspect(errors), 400);
	}

	var userId = req.user._id;
	var coords = [];
	coords[0] = parseFloat(req.param('long'));
	coords[1] = parseFloat(req.param('lat'));

	User.findById(userId, function (err, user) {
		if (err) {
			return handleError(res, err);
		}
		if (!user) {
			return res.send(404);
		}
		user.dateActive = new Date();
		user.isOnline = true;
		user.save();

		//console.log(user.criteria);

		UserLocation.findOne({user: userId}, function (err, location) {
			if (err) {
				return handleError(res, err);
			}
			if (!location) {
				location = new UserLocation();
			}

			// set longitude and latitude
			location.set('user', userId);
			location.set('geo', coords);
			location.set('updatedAt', Date.now());

			// save updated
			location.save(function (err) {

				if (err) {
					return handleError(res, err);
				}
				async.series([
					// update business
					function (cb) {
						Business.geoNear(coords, {
							spherical: true,
							distanceMultiplier: 3959,
							maxDistance: _.max(distances) / 3959
						}, function (err, oppts) {
							if (err) {
								return cb(err);
							}
							if (oppts.length === 0) {
								return cb();
							}
							User.populate(oppts, {
								path: 'obj.user'
							}, function (err, oppts) {
								if (err) {
									console.log(err);
									return cb(err);
								}
								async.each(oppts, function (oppt, callback) {
									oppt = oppt.obj;
									if (oppt.user) {
										var prevDist = 0;
										_.forEach(oppt.opportunity, function (item, i) {

											if (item.distance === prevDist) {
												item.distance *= 2;
											} else {
												prevDist = item.distance;
											}
											if (item.userGroup.indexOf(userId) === -1 &&
													geolib.isPointInCircle([coords[1], coords[0]], [oppt.geo[1], oppt.geo[0]], item.distance * mileToMi)) {
												item.userGroup.push(userId);
											}
											delete item._id;
										});
										oppt.save(function (err, oppt) {
											if (err) {
												return callback(err);
											}

											var data = [];

											_.forEach(oppt.opportunity, function (item) {
												data.push({
													distance: item.distance,
													count: item.userGroup.length
												});
											});

											// emit update to socket.
											if (oppt.user.socketId) {
												_.forEach(oppt.user.socketId, function (socketId) {
													global.io.sockets.socket(socketId).emit('opportunity:update', data);
												});
											}

											return callback();
										});
									} else {
										return callback();
									}
								}, function (err) {
									if (err) {
										return cb(err);
									}
									return cb();
								});
							});
						});
					},
					// update campaign
					function (cb) {
						Campaign.geoNear(coords, {
							spherical: true,
							distanceMultiplier: 3959,
							maxDistance: _.max(distances) / 3959,
							query: {active: true}
						}, function (err, campaigns) {
							if (err) {
								return cb(err);
							}

							if (campaigns.length === 0) {
								return cb(null, []);
							}

							User.populate(campaigns, {
								path: 'obj.user',
								select: '-socketId'
							}, function (err, campaigns) {
								if (err) {
									console.log(err);
									return cb(err);
								}
								async.filter(campaigns, function (campaign, callback) {
									campaign = campaign.obj;
									if (campaign.userGroup.indexOf(userId) === -1 && diffCriteria(campaign.criteria, user.criteria) && campaign.user) {
										return callback(true);
									} else {
										return callback(false);
									}
								}, function (results) {
									return cb(null, results);
								});
							});
						});
					},
					// remove from Business from what get away
					function (cb) {
						Business.geoNear(coords, {
							spherical: true,
							distanceMultiplier: 3959,
							maxDistance: 50 / 3959
						}, function (err, oppts) {
							if (err) {
								return cb(err);
							}
							//console.log(oppts);
							if (oppts.length === 0) {
								return cb();
							}
							User.populate(oppts, {
								path: 'obj.user'
							}, function (err, oppts) {
								if (err) {
									return cb(err);
								}
								async.each(oppts, function (oppt, callback) {
									oppt = oppt.obj;
									if (oppt.user) {
										var opportunity = [];
										_.forEach(oppt.opportunity, function (item) {
											//console.log(geolib.isPointInCircle([coords[1], coords[0]], [oppt.geo[1], oppt.geo[0]], item.distance * mileToMi));
											var userIndex = item.userGroup.indexOf(userId);
											opportunity.push({
												distance: item.distance,
												userGroup: item.userGroup
											});
											if (userIndex > -1 &&
													!geolib.isPointInCircle([coords[1], coords[0]], [oppt.geo[1], oppt.geo[0]], item.distance * mileToMi)) {

												item.userGroup = item.userGroup.splice(userIndex, 1);

											}
										});
										console.log(opportunity);
										oppt.opportunity = opportunity;
										oppt.save(function (err, oppt) {
											if (err) {
												return callback(err);
											}
											var data = [];
											_.forEach(oppt.opportunity, function (item) {
												data.push({
													distance: item.distance,
													count: item.userGroup.length
												});
											});
											// emit update to socket.
											if (oppt.user.socketId) {
												_.forEach(oppt.user.socketId, function (socketId) {
													global.io.sockets.socket(socketId).emit('opportunity:update', data);
												});
											}
											return callback();
										});
									} else {
										return callback();
									}
								}, function (err) {
									if (err) {
										return cb(err);
									}
									return cb();
								});
							});
						});
					}
				], function (err, results) {
					if (err) {
						return handleError(res, err);
					}
					return res.json(results[1]);
				});
			});
		});
	});
};


var updateBusinessOportunitiesManager = function (res, user) {
	Business.findOne({user: user._id}, function (err, business) {
		if (err) {
			return res.send(400, err);
		}

		var newOpportunity = [];
		_.forEach(business.opportunity, function (item) {
			newOpportunity.push({
				distance: item.distance,
				userGroup: []
			});
		});
		var coords = business.geo;
		UserLocation.geoNear(business.geo, {
			spherical: true,
			distanceMultiplier: 3959,
			maxDistance: _.max(distances) / 3959
		}, function (err, oppts) {
			if (err) {
				return res.send(400, err);
			}

			User.populate(oppts, {
				path: 'obj.user'
			}, function (err, oppts) {
				if (err) {
					return res.send(400, err);
				}

				async.each(oppts, function (oppt, callback) {
					oppt = oppt.obj;
					if (oppt.user && diffCriteria(oppt.user.criteria, user.criteria)) {
						_.forEach(newOpportunity, function (item) {
							if (item.userGroup.indexOf(oppt.user._id) === -1 &&
									geolib.isPointInCircle([coords[1], coords[0]], [oppt.geo[1], oppt.geo[0]], item.distance * mileToMi)) {
								item.userGroup.push(oppt.user._id);
							}
						});
					}
					return callback();
				}, function (err) {
					if (err) {
						return res.send(400, err);
					}
					business.set('opportunity', newOpportunity);
					//console.log(business);
					business.save(function (err) {
						if (err) {
							return res.send(400, err);
						}
					});
				});
			});
		});
	});
};

var updateBusinessOportunitiesUser = function (res, user) {
	//console.log('updateBusinessOportunitiesUser1');
	UserLocation.findOne({user: user._id}, function (err, location) {
		if (err) {
			return res.send(400, err);
		}
		if (!location || !location.geo) {
			return;
		}
		var coords = location.geo;
		Business.geoNear(location.geo, {
			spherical: true,
			distanceMultiplier: 3959,
			maxDistance: _.max(distances) / 3959
		}, function (err, oppts) {
			if (err) {
				return res.send(400, err);
			}

			User.populate(oppts, {
				path: 'obj.user'
			}, function (err, oppts) {
				if (err) {
					return res.send(400, err);
				}

				async.each(oppts, function (oppt, callback) {
					oppt = oppt.obj;
					if (oppt.user && diffCriteria(oppt.user.criteria, user.criteria)) {
						_.forEach(oppt.opportunity, function (item) {
							if (item.userGroup.indexOf(user._id) === -1 &&
									geolib.isPointInCircle([coords[1], coords[0]], [oppt.geo[1], oppt.geo[0]], item.distance * mileToMi)) {
								item.userGroup.push(user._id);
							}
							delete item._id;
						});
					}

					if (oppt.user && !diffCriteria(oppt.user.criteria, user.criteria)) {
						_.forEach(oppt.opportunity, function (item) {
							if (item.userGroup.indexOf(user._id) !== -1) {
								item.userGroup = _.without(item.userGroup, user._id);
							}
							delete item._id;
						});
					}
					console.log('updateBusinessOportunitiesUser2');
					oppt.save(function (err, oppt) {
						if (err) {
							return res.send(400, err);
						}
						var data = [];

						_.forEach(oppt.opportunity, function (item) {
							data.push({
								distance: item.distance,
								count: item.userGroup.length
							});
						});

						// emit update to socket.
						if (oppt.user && _.isArray(oppt.user.socketId)) {
							_.forEach(oppt.user.socketId, function (socketId) {
								global.io.sockets.socket(socketId).emit('opportunity:update', data);
							});
						}
					});
					return callback();
				}, function (err) {
					if (err) {
						return res.send(400, err);
					}
				});
			});
		});
	});
};


/**
 * Update user's criteria
 *
 * @param req
 * @param res
 * @param next
 */
exports.updateCriteria = function (req, res, next) {
	// parameter validation
	req.checkBody('criteria', 'Invalid criteria').notEmpty();
	var errors = req.validationErrors();
	if (errors) {
		return res.send('There have been validation errors: ' + util.inspect(errors), 400);
	}
	// function variables
	var userId = req.user._id;
	var criteria = req.param('criteria');
	if (typeof (criteria) === 'string') {
		criteria = JSON.parse(req.param('criteria'));
	}

	User.findById(userId, function (err, user) {
		if (err) {
			console.log(err);
			return handleError(res, err);
		}
		if (!user) {
			return res.send(404, {});
		}

		var oldCriteria = _.clone(user.criteria);

		user.set('criteria', criteria);
		user.save(function (err, user) {
			if (err) {
				console.log(err);
				return handleError(res, err);
			}
			var isChange = oldCriteria.length !== user.criteria.length;
			if (!isChange) {
				_.each(user.criteria, function (item) {
					if (!isChange) {
						var isFind = false;
						_.each(oldCriteria, function (oldItem) {
							if (item.text === oldItem.text) {
								isFind = true;
							}
						});
						if (!isFind) {
							isChange = true;
						}
					}
				});
			}
			if (isChange) {
				if (user.role === 'manager') {
					//updateBusinessOportunitiesManager(res, user);
				} else {
					//updateBusinessOportunitiesUser(res, user);
				}
			}

			return res.send(200, {});
		});
	});
};

exports.addCriteria = function (req, res) {
	var userId = req.user._id,
			criteria = req.param('criteria');
	// parameter validation
	req.checkBody('criteria', 'Invalid criteria').notEmpty();
	var errors = req.validationErrors();
	if (errors) {
		return res.send('There have been validation errors: ' + util.inspect(errors), 400);
	}
	if (typeof (criteria) === 'string') {
		criteria = JSON.parse(req.param('criteria'));
	}

	User.findById(userId, function (err, user) {
		var isCriteriaChanged = false;
		if (err) {
			console.log(err);
			return handleError(res, err);
		}
		if (!user) {
			return res.send(404, {});
		}

		_.each(criteria, function (newCr) {
			var crExists = _.find(user.criteria, function (oldCr) {
				return oldCr.text === newCr.text;
			});

			if (!crExists) {
				user.criteria.push({
					text: newCr.text
				});
				isCriteriaChanged = true;
			}
		});

		//user.set('criteria', newCriteria);
		user.save(function (err, user) {
			if (err) {
				console.log(err);
				return handleError(res, err);
			}
			if (isCriteriaChanged) {
				//updateBusinessOportunitiesUser(res, user);
			}

			return res.send(200, {});
		});
	});
};

exports.removeCriteria = function (req, res) {
	var userId = req.user._id,
			criteria = req.param('criteria');

	// parameter validation
	req.checkBody('criteria', 'Invalid criteria').notEmpty();
	var errors = req.validationErrors();
	if (errors) {
		return res.send('There have been validation errors: ' + util.inspect(errors), 400);
	}
	if (typeof (criteria) === 'string') {
		criteria = JSON.parse(req.param('criteria'));
	}

	User.findById(userId, function (err, user) {
		var isCriteriaChanged = false;
		if (err) {
			console.log(err);
			return handleError(res, err);
		}
		if (!user) {
			return res.send(404, {});
		}

		_.each(criteria, function (newCr) {
			var index = _.findIndex(user.criteria, function (oldCr) {
				return oldCr.text === newCr.text;
			});

			if (index !== -1) {
				user.criteria.splice(index, 1);
				isCriteriaChanged = true;
			}
		});

		//user.set('criteria', newCriteria);
		user.save(function (err, user) {
			if (err) {
				console.log(err);
				return handleError(res, err);
			}
			if (isCriteriaChanged) {
				// updateBusinessOportunitiesUser(res, user);
			}

			return res.send(200, {});
		});
	});
};

/**
 * Get my info
 */
exports.me = function (req, res, next) {
	var userId = req.user._id;
	console.log('me me me');
	User.findOne({
		_id: userId
	}, '-salt -hashedPassword -isOnline -socketId', function (err, user) { // don't ever give out the password or salt
		if (err)
			return next(err);
		if (!user)
			return res.json(401);
		Campaign.find({
			'userScanned.user': userId
		}).populate('user').exec(function (err, campaigns) {
			if (err)
				return next(err);

			async.map(campaigns, function (campaign, callback) {

				if (!campaign.user) {
					return callback(null, campaign);
				}

				Business.findOne({user: campaign.user._id}, function (err, business) {
					if (err) {
						return callback(err);
					}
					if (!business) {
						return callback(null, campaign);
					}
					campaign.set('business', business, {strict: false});
					callback(null, campaign);
				});
			}, function (err, campaigns) {
				if (err) {
					return next(err);
				}

				user.set('campaigns', campaigns, {strict: false});
				if (user.avatar) {
					user.set('avatar', true, {strict: false});
				} else {
					user.set('avatar', false, {strict: false});
				}

				if (user.role === 'manager') {
					Business.findOne({user: user._id}).lean().exec(function (err, business) {
						if (err) {
							return next(err);
						}
						/*
						if (!business) {
							return res.send(404, {});
						}*/
						if (!business) {
							business = {};
						}
						user.set('business', business, {strict: false});
						return res.json(user);
					});
				} else {
					UserLocation.findOne({user: user._id}, function (err, location) {
						if (err) {
							return next(err);
						}
						if (location && location.geo) {
							user.set('geo', location.geo, {strict: false});
						}
						return res.json(user);
					});
				}

			});

		});
	});
};

/**
 * Get business opportunities
 *
 * @param req
 * @param res
 * @param next
 */
exports.getOpportunities = function (req, res, next) {
	var userId = req.user._id;

	if (req.user.role == 'admin') {
		return res.json([]);
	}

	Business.findOne({user: userId}, function (err, oppt) {
		if (err) {
			return handleError(res, err);
		}

		if (!oppt) {
			return res.send([]);
		}
		var data = [];
		_.forEach(oppt.opportunity, function (item) {
			data.push({
				distance: item.distance,
				count: item.userGroup.length
			});
		});
		return res.json(data);
	});
};

exports.getOpportunitiesWithUsersData = function (req, res) {
	var userId = req.user._id;

	Business.findOne({user: userId})
			.populate({path: 'opportunity'})
			.exec(function (err, oppt) {
				if (err) {
					return handleError(res, err);
				}
				if (!oppt) {
					return res.send(404);
				}

				var options = {
					path: 'opportunity.userGroup',
					model: 'User'
				};
				Business.populate(oppt, options, function (err, bus) {
					if (err) {
						return handleError(res, err);
					}

					res.json(bus);
				});
			});
};

exports.getDailyDealsCount = function (req, res) {
	var userId = req.user._id;
	req.checkQuery('date', 'Invalid date').notEmpty();

	var errors = req.validationErrors();
	if (errors) {
		return res.send('There have been validation errors: ' + util.inspect(errors), 400);
	}

	var date = new Date(Date.parse(req.param('date')));

	Business.findOne({user: userId}, function (err, business) {
		if (err) {
			console.log(err);
			return handleError(res, err);
		}
		if (!business) {
			return res.send(404);
		}
		DailyDeal.findOne({
			'business': business._id,
			'date': {
				$gte: new Date(date.getFullYear(), date.getMonth(), date.getDate()),
				$lte: new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59, 999)
			}
		}).exec(function (err, dailydeal) {

			if (err) {
				return handleError(res, err);
			}
			if (!dailydeal) {
				return res.json({count: 0});
			}
			return res.json({count: dailydeal.usersScaned.length})
		});
	});
};

exports.getAvatar = function (req, res, next) {
	var userId = req.params.id || req.user._id;

	User.findById(userId, 'avatar', function (err, user) {
		if (err) {
			return next(err);
		}
		if (!user || !user.avatar) {
			return res.send(404);
		}
		res.sendfile(user.avatar.path);
	});
};

/**
 * Get criteria
 *
 * @param req
 * @param res
 * @param next
 */
exports.getCriteria = function (req, res, next) {
	var userId = req.user._id;

	User.findById(userId, 'criteria', function (err, user) {
		if (err) {
			return handleError(res, err);
		}
		if (!user) {
			return res.send(404);
		}

		return res.json(user.criteria);
	});
};


/**
 * Get campaign data
 *
 * @param req
 * @param res
 * @param next
 */
exports.getCampaign = function (req, res, next) {
	var userId = req.user._id;

	Campaign.findOne({
		user: userId,
		active: true
	}, function (err, campaign) {
		if (err) {
			return handleError(res, err);
		}
		if (!campaign) {
			Campaign.findOne({
				user: userId,
				status: 'moderate'
			}, function (err, campaign) {
				if (err) {
					return handleError(res, err);
				}
				if (!campaign) {
					Campaign.findOne({
						user: userId,
						status: 'reject'
					}).sort({'createdAt': 'desc'}).exec(function (err, campaign) {
						if (err) {
							return handleError(res, err);
						}
						if (!campaign) {
							return res.json([]);
						}
						return res.json([campaign]);
					});
				} else {
					return res.json([campaign]);
				}
			});
		} else {
			return res.json([campaign]);
		}
	});
};

/**
 * Get campaign data
 *
 * @param req
 * @param res
 * @param next
 */
exports.getPreviousCampaigns = function (req, res, next) {
	var userId = req.user._id;

	Campaign.find({
		user: userId,
		active: false,
		status: 'accept'
	}, function (err, campaigns) {
		if (err) {
			return handleError(res, err);
		}
		if (!campaigns) {
			return res.json([]);
		}
		return res.json(campaigns);
	});
};

/**
 * Get business near by user geo location
 *
 * @param req
 * @param res
 * @param next
 */
exports.getBusiness = function (req, res, next) {
	// parameter validation
	req.checkQuery('category', 'Invalid category').notEmpty();
	req.checkQuery('lat', 'Invalid latitude').notEmpty().isFloat();
	req.checkQuery('long', 'Invalid longitude').notEmpty().isFloat();
	//req.checkQuery('distance', 'Invalid distance').notEmpty().isFloat();

	var errors = req.validationErrors();
	if (errors) {
		return res.send('There have been validation errors: ' + util.inspect(errors), 400);
	}

	// function variables
	var category = req.param('category');
	var coords = [];
	coords[0] = parseFloat(req.param('long'));
	coords[1] = parseFloat(req.param('lat'));
	var distance = req.param('distance');

	/*Business.geoNear(coords, {
		spherical: true,
		maxDistance: parseFloat(distance / 3959),
		query: {
			businessCategory: category
		}
	}*/
	 Business.find({
		 businessCategory: category
	 }, function (err, businesses) {
		if (err) {
			console.log(err);
			return handleError(res, err);
		}
		if (businesses.length === 0) {
			return res.json([]);
		}
		//console.log(businesses);
		async.map(businesses, function (business, callback) {
			//console.log(business.user + ' Campaign');
			Campaign.findOne({user: business.user, active: true}).lean().exec(function (err, campaign) {
				var remainingTime = {
                    remainingHours: 0,
                    remainingMinutes: 0,
                    remainingSeconds: 0
                };

                if (err) {
					console.log(err);
					return callback(err);
				}

                if (campaign && campaign.active) {
                    var timeExpired = new Date() - campaign.createdAt,
                        remainingMilliseconds = campaign.hoursLimit * 60 * 60 * 1000 - timeExpired,
                        remainingHours, remainingMinutes, remainingSeconds, x;

                    if (remainingMilliseconds < 0) {
                        remainingHours = 0;
                        remainingMinutes = 0;
                        remainingSeconds = 0;
                    } else {
                        x = remainingMilliseconds / 1000;
                        remainingSeconds = Math.floor(x % 60);
                        x /= 60;
                        remainingMinutes = Math.floor(x % 60);
                        x /= 60;
                        remainingHours = Math.floor(x % 24);
                    }

                    remainingTime = {
                        remainingHours: remainingHours,
                        remainingMinutes: remainingMinutes,
                        remainingSeconds: remainingSeconds
                    };
                }

				var item = {
					campaign: campaign || {},
					distance: geolib.getDistance([coords[1], coords[0]], [business.geo[1], business.geo[0]]),
                    remainingTime: remainingTime
				};
				item = _.merge(item, business._doc);
				//console.log(item);
				//business.campaign = campaign || {};
				//business.distance = geolib.getDistance(coords, business.obj.geo);
				//console.log(item);
				callback(null, item);
			});
		}, function (err, result) {
			if (err) {
				console.log(err);
				return handleError(res, err);
			}
			return res.json(result);
		});
	});
};

// Update user online status
exports.updateOnlineStatus = function (userId, status, socketId, callback) {
	User.findById(userId, function (err, user) {
		if (err) {
			return callback(err);
		}
		if (!user) {
			return callback('User is not found');
		}
		if (user.role != 'user') {
			user.isOnline = status;
		}
		if (status) {
			user.socketId.push(socketId);
			console.log('added, socketid : %s', socketId);
		} else {
			var id = user.socketId.indexOf(socketId);
			console.log('deleting socket id from user table - id : %s, socketid : %s', id, socketId);
			user.socketId.splice(id, 1);
		}
		user.save(function (err, user) {
			if (err) {
				return callback(err);
			}
			return callback(null, user);
		});
	});
};

// Update user online status
exports.setUsersOffline = function (callback) {
	console.log('setUsersOffline');

	var dateCheck = new Date((new Date()).getTime() - 1000 * 60 * 30);
	var dateCheckStr = dateCheck.getUTCFullYear() + "-" +
			(dateCheck.getUTCMonth() < 9 ? "0" : "") + (dateCheck.getUTCMonth() + 1) + "-" +
			(dateCheck.getUTCDate() < 10 ? "0" + dateCheck.getUTCDate() : dateCheck.getUTCDate()) + " " +
			(dateCheck.getUTCHours() < 10 ? "0" + dateCheck.getUTCHours() : dateCheck.getUTCHours()) + ":" +
			(dateCheck.getUTCMinutes() < 10 ? "0" + dateCheck.getUTCMinutes() : dateCheck.getUTCMinutes());

	//var today = moment().startOf('day').subtract(30, 'minutes');
	//console.log(today.toDate());
	User.find({
		role: 'user',
		dateActive: {
			$lte: dateCheck
		}
	}, function (err, users) {

		_.each(users, function (user) {
			user.isOnline = false;
			user.save(function (err) {
				if (err) {
					console.log(err);
					return callback(err);
				}

				UserLocation.findOne({user: user._id}, function (err, location) {
					if (err) {
						return callback(err);
					}
					Business.geoNear(location.geo, {
						spherical: true,
						distanceMultiplier: 3959,
						maxDistance: 50 / 3959
					}, function (err, oppts) {
						if (err) {
							return callback(err);
						}
						if (oppts.length === 0) {
							return callback(null, user);
						}
						User.populate(oppts, {
							path: 'obj.user'
						}, function (err, oppts) {
							if (err) {
								return callback(err);
							}
							async.each(oppts, function (oppt, callback) {
								oppt = oppt.obj;
								if (oppt.user) {
									var opportunity = [];
									_.forEach(oppt.opportunity, function (item) {
										var userIndex = item.userGroup.indexOf(user._id);
										opportunity.push({
											distance: item.distance,
											userGroup: item.userGroup
										});
										if (userIndex > -1) {
											item.userGroup = item.userGroup.splice(userIndex, 1);
										}
									});
									oppt.opportunity = opportunity;
									oppt.save(function (err, oppt) {
										if (err) {
											return callback(err);
										}
										var data = [];
										_.forEach(oppt.opportunity, function (item) {
											data.push({
												distance: item.distance,
												count: item.userGroup.length
											});
										});
										// emit update to socket.
										if (oppt.user.socketId) {
											_.forEach(oppt.user.socketId, function (socketId) {
												global.io.sockets.socket(socketId).emit('opportunity:update', data);
											});
										}
										return callback();
									});
								} else {
									return callback();
								}
							}, function (err) {
								if (err) {
									return callback(err);
								}
								return  callback(null, user);
							});
						});
					});
				});

			});
		});

	});
};



exports.uploadImage = function (req, res, next) {
	var userId = req.user._id;
	if (req.files.file && req.files.file.size > 0) {

		fs.readFile(req.files.file.path, function (err, data) {
			if (err) {
				return handleError(res, err);
			}
			var newPath = __dirname + "/../../uploads/fullsize/" + req.files.file.name;
			/// write file to uploads/fullsize folder
			fs.writeFile(newPath, data, function (err) {
				if (err) {
					return handleError(res, err);
				}
				fs.unlink(req.files.file.path);
				Business.findOne({user: userId}, function (err, business) {
					fs.unlink(__dirname + "/../../uploads/fullsize/" + business.imageName);
					business.set('imageName', req.files.file.name);
					business.save(function (err) {
						if (err) {
							return handleError(res, err);
						}
						return res.json(200, {imageName: req.files.file.name});
					});
				});
			});
		});
	}
};


exports.showImage = function (req, res, next) {
	var imageName = req.params.name;
	var img = fs.readFileSync(__dirname + "/../../uploads/fullsize/" + imageName);
	res.writeHead(200, {'Content-Type': 'image/jpg'});
	res.end(img, 'binary');
};

/**
 *
 * @param {type} req
 * @param {type} res
 * @param {type} next
 * @returns {undefined}Register device token for mobile user
 */
exports.registerDeviceToken = function (req, res, next) {
	var userId = req.user._id;
	User.findById(userId, function (err, user) {
		user.set('deviceToken', req.body.deviceToken);
		user.save(function (err) {
			if (err) {
				return res.send(500, err);
			}
			return res.send(200, {ok: true});
		});
	});
};

exports.clearDailyDeal = function (req, res, next) {
	var userId = req.user._id;
	Business.findOne({user: userId}, function (err, business) {
		if (err) {
			return handleError(res, err);
		}
		DailyDeal.remove({business: business._id}, function(err){
			if (err) {
				return handleError(res, err);
			}
			business.totalDailyDealScan = 0;
			business.save(function (err, b) {
				if (err) {
				  return handleError(res, err);
				}
				return res.send(200, {ok: true});
			});
		});
	});
};

/**
 * Authentication callback
 */
exports.authCallback = function (req, res, next) {
	res.redirect('/');
};

exports.flipnow = function (req, res, next) {
	console.log('real-time update api verification : ', req);
	if (req.param('hub.verify_token') === 'flipnow') {
		res.send(req.param('hub.challenge'));
	}
};

exports.receive_flipnow = function (req, res, next) {
	console.log('received real-time update :', JSON.stringify(req.body));
};

function handleError(res, err) {
	return res.send(500, err);
}
