'use strict';

var express = require('express');
var controller = require('./user.controller');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/flipnow', controller.flipnow);
router.post('/flipnow', controller.receive_flipnow);

router.get('/', auth.hasRole('admin'), controller.index);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.get('/me', auth.isAuthenticated(), controller.me);
router.get('/me/opportunities', auth.isAuthenticated(), controller.getOpportunities);
router.get('/me/opportunities-with-users-data', auth.isAuthenticated(), controller.getOpportunitiesWithUsersData);
router.get('/me/getDailyDealsCount', auth.isAuthenticated(), controller.getDailyDealsCount);
router.get('/me/avatar', auth.isAuthenticated(), controller.getAvatar);
router.get('/:id/criteria', auth.isAuthenticated(), controller.getCriteria);
router.get('/:id/avatar', controller.getAvatar);
router.get('/:id/campaign', auth.isAuthenticated(), controller.getCampaign);
router.get('/:id/previous-campaigns', auth.isAuthenticated(), controller.getPreviousCampaigns);
router.get('/:id/business', auth.isAuthenticated(), controller.getBusiness);
router.put('/me', auth.isAuthenticated(), controller.update);
router.put('/:id/password', auth.isAuthenticated(), controller.changePassword);
router.put('/:id/location', auth.isAuthenticated(), controller.updateLocation);
router.put('/:id/criteria', auth.isAuthenticated(), controller.updateCriteria);
router.put('/me/add-criteria', auth.isAuthenticated(), controller.addCriteria);
router.put('/me/remove-criteria', auth.isAuthenticated(), controller.removeCriteria);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', controller.create);
router.put('/me/checkout', auth.isAuthenticated(), controller.checkout);
router.get('/me/clear-daily-deal', auth.isAuthenticated(), controller.clearDailyDeal);
router.put('/me/sendsupport', auth.isAuthenticated(), controller.sendSupport);
router.get('/me/check-paid', auth.isAuthenticated(), controller.checkIsPaid);
router.post('/me/upload-image', auth.isAuthenticated(), controller.uploadImage);
router.get('/image/:name', controller.showImage);
router.put('/me/register-device-token', auth.isAuthenticated(), controller.registerDeviceToken);


router.post('/me/change-email', auth.isAuthenticated(), controller.changeEmailStep1);
router.put('/set-new-email', controller.changeEmailStep2);

router.post('/recover-password', controller.recoverPasswordStep1);
router.put('/set-new-password', controller.recoverPasswordStep2);
//router.get('/me/confirm-email', auth.isAuthenticated(), controller.confirmEmail);
router.put('/check-confirm-email', controller.checkConfirmEmail);

module.exports = router;
