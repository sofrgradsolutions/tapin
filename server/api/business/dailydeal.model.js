'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var DailydealSchema = new Schema({
  business: {type: Schema.ObjectId, ref: 'Business'},
  date: Date,
  usersScanedCount: {type: Number, default: 0},
  usersScaned: [{
    user: {type: Schema.ObjectId, ref: 'User'}
  }]
});

module.exports = mongoose.model('Dailydeal', DailydealSchema);
