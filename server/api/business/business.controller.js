'use strict';

var User = require('../user/user.model');
var passport = require('passport');
var config = require('../../config/environment');
var _ = require('lodash');
var async = require('async');
var distances = config.distances;
var util = require('util');
var UserLocation = require('../user/user-location.model');
var Business = require('../user/business.model');
var Campaign = require('../campaign/campaign.model');
var DailyDeal = require('./dailydeal.model');
var geolib = require('geolib');
var mileToMi = 1609.34;
var mileToKm = 1.60934;
var radianToMile = 3959;

var validationError = function (res, err) {
	return res.json(422, err);
};

/**
 * Get list of trending business
 */
exports.getTrending = function (req, res) {
	var userId = req.user._id;
	var date = new Date();
	UserLocation.findOne({user: userId}, function (err, userLocation) {
		if (err) {
			return handleError(res, err);
		}
		if (!userLocation) {
			return res.send(404);
		}

		DailyDeal.find(
			{
				usersScanedCount: {$gte: 3},
				'date': {
				  $gte: new Date(date.getFullYear(), date.getMonth(), date.getDate()),
				  $lte: new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59, 999)
				}
			}, function (err, dailyDeals) {
			if (err) {
				return handleError(res, err);
			}

			if (dailyDeals.length === 0) {
				return res.json([]);
			}

			async.map(dailyDeals, function (dailyDeal, callback) {
				//console.log(dailyDeal);
				Business.findOne({_id: dailyDeal.business}, '-cardnumber -cardholderName -expYear -expMonth').populate('business').exec(function (err, business) {
					//console.log(business);
					if (err) {
						return callback(err);
					}
					if (!business) {
						return callback();
					}
					
					return callback(null, business._doc);
				});
			}, function (err, results) {
				if (err) {
					return handleError(res, err);
				}
				console.log('*** returning trending business *** :', results);
				return res.json(results);
			});
		});
	});
};

exports.getBusinessById = function (req, res) {
	var date = new Date();
	Business.findOne({_id: req.params.id}, '-cardnumber -cardholderName -expYear -expMonth', function (err, business) {
		if (err) {
		  return handleError(res, err);
		}

		Campaign.findOne({user: business.user, active: true}, function (err, campaign) {
			var remainingTime = {
				remainingHours: 0,
				remainingMinutes: 0,
				remainingSeconds: 0
			};

			if (err) {
				return handleError(res, err);
			}
			if (campaign && campaign.active) {
				var timeExpired = new Date() - campaign.createdAt,
					remainingMilliseconds = campaign.hoursLimit * 60 * 60 * 1000 - timeExpired,
					remainingHours, remainingMinutes, remainingSeconds, x;

				if (remainingMilliseconds < 0) {
					remainingHours = 0;
					remainingMinutes = 0;
					remainingSeconds = 0;
				} else {
					x = remainingMilliseconds / 1000;
					remainingSeconds = Math.floor(x % 60);
					x /= 60;
					remainingMinutes = Math.floor(x % 60);
					x /= 60;
					remainingHours = Math.floor(x % 24);
				}

				remainingTime = {
					remainingHours: remainingHours,
					remainingMinutes: remainingMinutes,
					remainingSeconds: remainingSeconds
				};
			}

			DailyDeal.findOne({
				'business': business._id,
				'date': {
				  $gte: new Date(date.getFullYear(), date.getMonth(), date.getDate()),
				  $lte: new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59, 999)
				}
			  }).exec(function(err, dailydeal) {
					if (err) {
					   return handleError(res, err);
					}
					var data = _.clone(business._doc);
					data.campaign = campaign;
					data.todayDailyDealScan = _.isObject(dailydeal) ? dailydeal.usersScaned.length : 0;
                    data.remainingTime = remainingTime;
					return res.json(data);
			  });

		});
	});
};

exports.scan = function (req, res) {
	var businessId = req.params.id,
		userId = req.user._id,
		currentDate = new Date();
	//console.log(businessId);
	//console.log(currentDate);
	if (!businessId || !userId) {
	  return res.send(404, {});
	}

	Business.findOne({'token': req.params.id}).exec(function (err, business) {
		if (err) {
		  return handleError(res, err);
		}
	    //console.log(new Date(currentDate));
		DailyDeal.findOne({
			'business': business._id,
			'date': {
			  $gte: new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()),
			  $lte: new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(), 23, 59, 59, 999)
			}
		  }).exec(function (err, dailydeal) {
			  if (err) {
				//console.log(err);
				return handleError(res, err);
			  }
			  var ok = true;
			  var error = '';
			  //console.log(dailydeal);
			  if (!dailydeal) {
				dailydeal = new DailyDeal();
				dailydeal.date = currentDate;
				dailydeal.business = business._id;
				dailydeal.usersScaned.push({
				  user: userId
				});
				dailydeal.usersScanedCount = dailydeal.usersScaned.length;

			  } else if (!_.findWhere(dailydeal.usersScaned, {user: userId})) {
				dailydeal.usersScaned.push({
				  user: userId
				});
				dailydeal.usersScanedCount = dailydeal.usersScaned.length;
			  } else {
				ok = false;
			    error = 'already_exists';
			  }


			  dailydeal.save(function (err, dd) {
				if (err) {
				  return handleError(res, err);
				}
				if (ok) {
					business.totalDailyDealScan = business.totalDailyDealScan + 1;
				}
				business.save(function (err, b) {
					if (err) {
					  return handleError(res, err);
					}

					return res.send(200, {ok: ok, count: dd.usersScaned.length, error: error});
				});
			  });
		  });
	});

};

function handleError(res, err) {
	return res.send(500, err);
}
