var Promocode = require('./promocode.model');

exports.index = function (req, res) {
    Promocode.find({deleted: {$exists: false}}, function (err, codes) {
        if (err) {
            return res.send(500, err);
        }
        res.json(200, codes);
    })
};

exports.get = function (req, res) {
    Promocode.findOne({_id: req.params.id, deleted: {$exists: false}}, function(err, code) {
        if (err) {
            return res.send(500, err);
        }
        if (!code) {
            return res.send(404);
        } else {
            res.json(200, code);
        }
    })
};

exports.create = function (req, res) {
    Promocode.find({name: req.body.name, deleted: {$exists: false}}, function (err, code) {
        if (err) {
            return res.send(500, err);
        }
        if (code.length > 0) {
            return res.send('Promocode with name ' + req.body.name + ' is already exists', 400);
        } else {
            var newPromocode = new Promocode(req.body);
            newPromocode.creationDate = new Date();
            newPromocode.save(function (err, code) {
                if (err) {
                    return res.send(500, err);
                }
                return res.json(200, code);
            });
        }
    });
};

exports.update = function (req, res) {
    Promocode.findOne({_id: req.body.code._id}, function (err, code) {
        if (err) {
            return res.send(500, err);
        }
        if (!code) {
            return res.send(404);
        }
        Promocode.find({name: req.body.code.name, deleted: {$exists: false}, _id: {$ne: req.body.code._id}}, function (err, codeWithName) {
            if (err) {
                return res.send(500, err);
            }
            if (codeWithName.length > 0) {
                return res.send('Promocode with name ' + req.body.code.name + ' is already exists', 400);
            } else {
                code.name = req.body.code.name;
                code.duration = req.body.code.duration;
                code.blastLimit = req.body.code.blastLimit;
                code.save(function (err) {
                    if (err) {
                        return res.send(500, err);
                    }
                    return res.json(200, code);
                });
            }
        });
    });
};

exports.remove = function(req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Promocode.findOne({_id: req.params.id}, function (err, code) {
        if (err) {
            return res.send(500, err);
        }
        if (!code) {
            return res.send(404);
        }
        code.deleted = 'deleted';
        code.save(function (err) {
            if (err) {
                return res.send(500, err);
            }
            return res.json(200, {deleted: 'success'});
        });
    });
};
