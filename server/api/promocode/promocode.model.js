'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PromocodeSchema = new Schema({
    name: String,
    duration: String,
    blastLimit: Number,
    creationDate: Date,
    deleted: String
});

module.exports = mongoose.model('Promocode', PromocodeSchema);
