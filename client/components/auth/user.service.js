'use strict';

angular.module('tapinApp')
	.factory('User', function ($resource) {
		return $resource('/api/users/:id/:controller', {
			id: '@_id'
		},
		{
			changePassword: {
				method: 'PUT',
				params: {
					controller: 'password'
				}
			},
			confirmEmail: {
				method: 'GET',
				params: {
					id: 'me',
					controller: 'confirm-email'
				}
			},
			checkConfirmEmail: {
				method: 'PUT',
				params: {
					controller: 'check-confirm-email'
				}
			},
			setNewPassword: {
				method: 'PUT',
				params: {
					controller: 'set-new-password'
				}
			},
			update: {
				method: 'PUT',
				params: {
					id: 'me'
				}
			},
			checkout: {
				method: 'PUT',
				params: {
					controller: 'checkout',
					id: 'me'
				}
			},
			sendSupportMessage: {
				method: 'PUT',
				params: {
					controller: 'sendsupport',
					id: 'me'
				}
			},
			updateLocation: {
				isArray: true,
				method: 'PUT',
				params: {
					controller: 'location',
					id: 'me'
				}
			},
			updateCriteria: {
				method: 'PUT',
				params: {
					controller: 'criteria',
					id: 'me'
				}
			},
			getBusiness: {
				method: 'GET',
				params: {
					controller: 'business',
					id: 'me'
				}
			},
			get: {
				method: 'GET',
				params: {
					id: 'me'
				}
			},
			getOpportunities: {
				method: 'GET',
				isArray: true,
				params: {
					id: 'me',
					controller: 'opportunities'
				}
			},
			getOpportunitiesWithUsersData: {
				method: 'GET',
				params: {
					id: 'me',
					controller: 'opportunities-with-users-data'
				}
			},
			getDailyDealsCount: {
				method: 'GET',
				params: {
					id: 'me',
					controller: 'getDailyDealsCount'
				}
			},
			getCriteria: {
				method: 'GET',
				isArray: true,
				params: {
					id: 'me',
					controller: 'criteria'
				}
			},
			getCampaign: {
				method: 'GET',
				isArray: true,
				params: {
					id: 'me',
					controller: 'campaign'
				}
			},
			getPreviousCampaigns: {
				method: 'GET',
				isArray: true,
				params: {
					id: 'me',
					controller: 'previous-campaigns'
				}
			},
			checkIsPaid: {
				method: 'GET',
				params: {
					id: 'me',
					controller: 'check-paid'
				}
			},
			changeEmailStep1: {
				method: 'POST',
				params: {
					id: 'me',
					controller: 'change-email'
				}
			},
			setNewEmail: {
				method: 'PUT',
				params: {
					controller: 'set-new-email'
				}
			},
			clearDailyDeal: {
				method: 'GET',
				params: {
					id: 'me',
					controller: 'clear-daily-deal'
				}
			}
		});
	})
	.factory('Business', function ($resource) {
		return $resource('/api/businesses/:controller/:id', {
			id: '@_id'
		},
		{
			trending: {
				method: 'GET',
				params: {
					controller: 'trending'
				}
			},
			testShow: {
				method: 'GET',
				params: {
					controller: 'show'
				}
			}
		});
	});
