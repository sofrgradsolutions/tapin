'use strict';

angular.module('tapinApp')
		
		.config(function (localStorageServiceProvider) {
			localStorageServiceProvider.setPrefix('TapN').setStorageType('sessionStorage');
		})

		.factory('sessionStore', function sessionStore($cookieStore, localStorageService) {

			/*
			if (localStorageService.isSupported) {
				console.log('supported');
			}
			*/

			return {
				put: function (key, val) {
					if (!localStorageService.set(key, val)) {
						console.log('not set:' + val);
					}
					//$cookieStore.put(key, val);
				},
				get: function (key) {
					return localStorageService.get(key);
					//return $cookieStore.get(key);
				},
				remove: function (key) {
					localStorageService.remove(key);
					//$cookieStore.remove(key);
				}
			};
		});
