'use strict';

angular.module('tapinApp')
		.factory('Auth', function Auth($location, $rootScope, $http, User, sessionStore, $q) {
			var currentUser = {};
			if (sessionStore.get('token')) {
				currentUser = User.get();
			}

			return {
				/**
				 * Authenticate user and save token
				 *
				 * @param  {Object}   user     - login info
				 * @param  {Function} callback - optional
				 * @return {Promise}
				 */
				login: function (user, callback) {
					var cb = callback || angular.noop;
					var deferred = $q.defer();

					$http.post('/auth/local', {
						email: user.email,
						password: user.password
					})
					.success(function (data) {
						if (data.role == 'manager' || data.role == 'admin') {
							sessionStore.put('token', data.token);
							sessionStore.put('step', data.role == 'admin' ? 'finish' : data.step);
							currentUser = User.get();							
							deferred.resolve();
							return cb();
						} else {
							deferred.reject({message: 'Site access is restricted to only business users.'});
							return cb({message: 'Site access is restricted to only business users.'});
						}
					})
					.error(function (err) {
						this.logout();
						deferred.reject(err);
						return cb(err);
					}.bind(this));

					return deferred.promise;
				},

				/**
				 * Authenticate user and save token
				 *
				 * @param  {Object}   user     - login info
				 * @param  {Function} callback - optional
				 * @return {Promise}
				 */
				recover: function (user, callback) {
					var cb = callback || angular.noop;
					var deferred = $q.defer();
					$http.post('/api/users/recover-password', {
						email: user.email
					})
					.success(function (data) {
						deferred.resolve();
						return cb();
					})
					.error(function (err) {
						deferred.reject(err);
						return cb(err);
					}.bind(this));
					return deferred.promise;
				},

				/**
				 * Delete access token and user info
				 *
				 * @param  {Function}
				 */
				logout: function () {
					sessionStore.remove('token');
					currentUser = {};
				},
				/**
				 * Create a new user
				 *
				 * @param  {Object}   user     - user info
				 * @param  {Function} callback - optional
				 * @return {Promise}
				 */
				createUser: function (user, callback) {
					var cb = callback || angular.noop;

					return User.save(user,
							function (data) {
								//sessionStore.put('token', data.token);
								sessionStore.put('step', user.step);
								//currentUser = User.get();
								return cb(user);
							},
							function (err) {
								this.logout();
								return cb(err);
							}.bind(this)).$promise;
				},
				/**
				 * Change password
				 *
				 * @param  {String}   oldPassword
				 * @param  {String}   newPassword
				 * @param  {Function} callback    - optional
				 * @return {Promise}
				 */
				changePassword: function (oldPassword, newPassword, callback) {
					var cb = callback || angular.noop;

					return User.changePassword({id: currentUser._id}, {
						oldPassword: oldPassword,
						newPassword: newPassword
					}, function (user) {
						return cb(user);
					}, function (err) {
						return cb(err);
					}).$promise;
				},
				/**
				 * Gets all available info on authenticated user
				 *
				 * @return {Object} user
				 */
				getCurrentUser: function () {
					return currentUser;
				},
				setCurrentUser: function () {
					currentUser = User.get();
				},
				/**
				 * Check if a user is logged in
				 *
				 * @return {Boolean}
				 */
				isLoggedIn: function () {
					return currentUser.hasOwnProperty('role');
				},
				/**
				 * Waits for currentUser to resolve before checking if user is logged in
				 */
				isLoggedInAsync: function (cb) {
					if (currentUser.hasOwnProperty('$promise')) {
						currentUser.$promise.then(function () {
							cb(true);
						}).catch(function () {
							cb(false);
						});
					} else if (currentUser.hasOwnProperty('role')) {
						cb(true);
					} else {
						cb(false);
					}
				},
				/**
				 * Check if a user is an admin
				 *
				 * @return {Boolean}
				 */
				isAdmin: function () {
					return currentUser.role === 'admin';
				},
				/**
				 * Get auth token
				 */
				getToken: function () {
					return sessionStore.get('token');
				},
				getStep: function () {
					return sessionStore.get('step');
				},
				setStep: function (step) {
					return sessionStore.put('step', step);
				}
			};
		});
