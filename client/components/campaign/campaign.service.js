'use strict';

angular.module('tapinApp')
  .factory('Campaign', function ($resource) {
    return $resource('/api/campaigns/:id/:controller', {
        id: '@_id'
      },
      {
        get: {
          method: 'GET',
          isArray: true
        },
        getNonModerated: {
          method: 'GET',
          isArray: true,
          params: {
            id: 'me',
            controller: 'getNonModerated'
          }
        },
        save: {
          method: 'POST'
        },
        accept: {
          method: 'GET',
          params: {
            controller: 'accept'
          }
        },
        reject: {
          method: 'GET',
          params: {
            controller: 'reject'
          }
        },
        getCriteria: {
          method: 'GET',
          isArray: true,
          params: {
            id: 'me',
            controller: 'getCriteriaList'
          }
        }
      });
  });
