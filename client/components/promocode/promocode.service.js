'use strict';

angular.module('tapinApp')
    .factory('Promocode', function ($resource) {
        return $resource('/api/promocodes/:id/:controller', {
                id: '@_id'
            },
            {
                getAll: {
                    method: 'GET',
                    isArray: true
                },
                get: {
                    method: 'GET'
                },
                create: {
                    method: 'POST'
                },
                update: {
                    method: 'PUT'
                },
                delete: {
                    method: 'DELETE'
                }
            });
    });
