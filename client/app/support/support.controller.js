'use strict';

angular.module('tapinApp')
	.controller('SupportCtrl', function ($scope, $location, User, bootbox) {
		$scope.submitted = false;
		$scope.form = {};
		$scope.sendMessage = function () {
			$scope.submitted = true;
			$scope.error = '';
			if ($scope.form.$valid) {
				User.sendSupportMessage($scope.support, function () {
					bootbox.alert("Your message has been successfully sent.");
					$scope.support.message = "";
					$scope.submitted = false;
				}, function (err) {
					if (err) {
						$scope.error = err.statusText;
					}
				});
			}
		};
	});
