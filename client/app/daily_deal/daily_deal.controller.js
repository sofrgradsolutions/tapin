'use strict';

angular.module('tapinApp')
	.controller('DailyDueCtrl', function ($scope, User, user, bootbox) {
		$scope.user = user;
		$scope.user.businessCategory = $scope.user.business.businessCategory;
		$scope.usersNumber = 0;

		var today = new Date();
		$scope.date = (today.getMonth() < 9 ? "0" : "") + (today.getMonth() + 1) + "/" +
				(today.getDate() < 10 ? "0" + today.getDate() : today.getDate()) + "/" +
				today.getFullYear();

		$scope.save = function () {
			if (!$scope.user.business.businessName.$error) {
				delete $scope.user.__v;
				delete $scope.user._id;
				delete $scope.user.business.__v;
				delete $scope.user.business._id;

				User.update($scope.user, function () {
					User.clearDailyDeal();
					$scope.usersNumber = 0;
					bootbox.alert('User information successfully changed.');
				});
			}
		};

		$scope.checkTapinUsers = function () {
			User.getDailyDealsCount({date: $scope.date}, function (data) {
				$scope.usersNumber = data.count;
			});
		};

		$scope.checkTapinUsers();
	});
