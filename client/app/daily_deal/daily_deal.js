'use strict';

angular.module('tapinApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('daily_deal', {
        url: '/daily_deal',
        templateUrl: 'app/daily_deal/daily_deal.html',
        controller: 'DailyDueCtrl',
        resolve: {
          user: function (User) {
            return User.get().$promise;
          }
        }
      });
  });
