'use strict';

angular.module('tapinApp')
    .controller('AdminCtrl', function ($scope, $http, Auth, User) {

        // Use the User $resource to fetch all users
        $scope.users = User.query();
        $scope.Auth = Auth;

        $scope['delete'] = function (user) {
            User.remove({id: user._id});
            angular.forEach($scope.users, function (u, i) {
                if (u === user) {
                    $scope.users.splice(i, 1);
                }
            });
        };
    })
    .controller('AdminCampaignCtrl', function ($scope, $http, Auth, Campaign, bootbox, User) {
        $scope.campaigns = Campaign.getNonModerated();
        $scope.Auth = Auth;

        var acceptCampaign = function (campaign, index) {
            Campaign.accept({id: campaign.campaign._id}, function () {
                $scope.campaigns.splice(index, 1);
                bootbox.alert('Campaign for ' + campaign.business.businessName + ' has been successfully accepted');
            }, function (err) {
                $scope.error = err.statusText;
            });
        };

        var rejectCampaign = function (campaign, index, reason) {
            Campaign.reject({id: campaign.campaign._id}, function () {
                $scope.campaigns.splice(index, 1);
                var msg = 'Campaign for ' + campaign.business.businessName + ' has been rejected. ';
                if (reason) {
                    msg += reason;
                }
                bootbox.alert(msg);
            }, function (err) {
                $scope.error = err.statusText;
            });
        };

        $scope.accept = function (index, campaign) {
            if (campaign.campaign.price === 0) {
                acceptCampaign(campaign, index);
            } else {
                User.checkout({amount: campaign.campaign.price, user: campaign.campaign.user},
                    function () {
                        acceptCampaign(campaign, index);
                    },
                    function (err) {
                        //$scope.errors = err.data.errors.raw.message;
                        rejectCampaign(campaign, index, err.data.errors.raw.message);
                    });
            }
        };

        $scope.reject = function (index, campaign) {
            rejectCampaign(campaign, index);
        }
    })
    .controller('AdminPromocodesCtrl', function($scope, Promocode) {
        $scope.error = '';
        $scope.promocodes = Promocode.getAll();

        $scope.remove = function(code) {
            Promocode.delete({id: code._id}, function() {
                bootbox.alert('Promo code ' + code.name + ' has been removed');
                var deletedCodeIdx = _.findIndex($scope.promocodes, {_id: code._id});
                $scope.promocodes.splice(deletedCodeIdx, 1);
            }, function(err) {
                if (err) {
                    $scope.error = err.data;
                }
            });
        };
    })
	.controller('AdminPromocodesFormCtrl', function($scope, $location, $stateParams, Promocode) {
        $scope.durationItems = ['1 month', '3 month', '6 month', '12 month'];
        $scope.error = '';
        if ($stateParams.id) {
            $scope.form = Promocode.get({id: $stateParams.id});
            $scope.method = 'update';
        } else {
            $scope.form = {
                name: '',
                duration: '1 month',
                blastLimit: 0
            };
            $scope.method = 'create';
        }

        $scope.create = function() {
            Promocode.create($scope.form, function(){
                bootbox.alert('Your promo code has been successfully created');
                $location.path('/promocodes');
            }, function(err) {
                if (err) {
                    $scope.error = err.data;
                }
            });
        };

        $scope.update = function() {
            Promocode.update({code: $scope.form}, function() {
                bootbox.alert('Your promo code has been successfully updated');
                $location.path('/promocodes');
            }, function(err) {
                if (err) {
                    $scope.error = err.data;
                }
            });
        };
    });
