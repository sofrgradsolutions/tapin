'use strict';

angular.module('tapinApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('admin', {
                url: '/admin',
                templateUrl: 'app/admin/admin.html',
                controller: 'AdminCtrl'
            })
            .state('moderate_campaigns', {
                url: '/moderate_campaigns',
                templateUrl: 'app/admin/campaigns.html',
                controller: 'AdminCampaignCtrl'
            })
            .state('promocodes', {
                url: '/promocodes',
                templateUrl: 'app/admin/promocodes/list.html',
                controller: 'AdminPromocodesCtrl'
            })
            .state('promocodeForm', {
                url: '/promocodes/form/:id',		
                templateUrl: 'app/admin/promocodes/form.html',
                controller: 'AdminPromocodesFormCtrl'
            });
    });
