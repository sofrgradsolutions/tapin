'use strict';

angular.module('tapinApp')    
    .controller('AdminPromocodesCtrl', function($scope, Promocode) {
        $scope.error = '';
        $scope.promocodes = Promocode.getAll();

        $scope.remove = function(code) {
			bootbox.confirm("Are you sure to delete?", function(result) {
				if (result) {
					Promocode.delete({id: code._id}, function() {
						bootbox.alert('Promo code ' + code.name + ' has been removed');
						var deletedCodeIdx = _.findIndex($scope.promocodes, {_id: code._id});
						$scope.promocodes.splice(deletedCodeIdx, 1);
					}, function(err) {
						if (err) {
							$scope.error = err.data;
						}
					});
				}
			});
        };
    })
	.controller('AdminPromocodesFormCtrl', function($scope, $location, $stateParams, Promocode) {
        $scope.durationItems = ['1 month', '3 month', '6 month', '12 month'];
        $scope.error = '';

        if ($stateParams.id) {
            $scope.form = Promocode.get({id: $stateParams.id});
            $scope.method = 'updated';
        } else {
            $scope.form = {
                name: '',
                duration: '1 month',
                blastLimit: 0
            };
            $scope.method = 'created';
        }

        $scope.save = function() {			
			var success = function(){
                bootbox.alert('Your promo code has been successfully ' + $scope.method);
                $location.path('/promocodes');
            };			
			var failed = function(err) {
                if (err) {
                    $scope.error = err.data;
                }
            };

			if ($scope.method === 'created') {
				Promocode.create($scope.form, success, failed);
			} else {
				Promocode.update({code: $scope.form}, success, failed);
			}
        };
    });
