'use strict';

angular.module('tapinApp')
	.controller('CampaignsCtrl', function ($scope, previousCampaigns) {
		$scope.previousCampaigns = previousCampaigns;
	});
