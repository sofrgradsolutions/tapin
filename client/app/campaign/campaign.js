'use strict';

angular.module('tapinApp')
	.config(function ($stateProvider) {
		var criteriaList = [
			{text: 'Vegan'}, {text: 'Mountain'}, {text: 'Clothingsales'},
			{text: 'Clubs'}, {text: 'Rockmusic'},
			{text: 'Bars'}, {text: 'Happyhour'}, {text: 'Male'},
			{text: 'Wine'}, {text: 'Beach'}, {text: 'Dancing'},
			{text: 'Health'}, {text: 'Female'}, {text: 'Green Juice'}
		];

		$stateProvider
			.state('campaigns', {
				url: '/campaigns',
				templateUrl: 'app/campaigns/campaigns.html',
				controller: 'CampaignsCtrl',
				resolve: {
					criteria: function (User) {
						return criteriaList;//User.getCriteria().$promise;
					},
					opportunities: function (User) {
						return User.getOpportunities().$promise;
					},
					campaign: function (User) {
						return User.getCampaign().$promise;
					},
					previousCampaigns: function (User) {
						return User.getPreviousCampaigns().$promise;
					}
				}
			})
			.state('campaign', {
				url: '/campaign',
				templateUrl: 'app/campaign/campaign.html',
				controller: 'CampaignCtrl',
				resolve: {
					checkPaid: function (User) {
						return User.checkIsPaid().$promise;
					},
					criteria: function (User) {
						return criteriaList;//User.getCriteria().$promise;
					},
					business: function (User) {
                        return User.getOpportunitiesWithUsersData().$promise;
					},
					campaign: function (User) {
						return User.getCampaign().$promise;
					},
					user: function (User) {
						return User.get().$promise;
					},
          companyCriteria: function(Campaign) {
            return Campaign.getCriteria().$promise;
          }
				}
			});

	});
