'use strict';

angular.module('tapinApp')
    .controller('CampaignCtrl', function ($scope, business, criteria, Campaign, $state, campaign, checkPaid, user, User, numberFilter, bootbox, companyCriteria) {
        $scope.form = {
            priceByUser: 0.37,
            price: 0,
            criteria: [],
            age: [],
            gender: [],
            limit: 30,
            isSetHoursLimit: false,
            hoursLimit: 0,
            step: 1
        };
		$scope.available = 0;

        $scope.business = business;
        $scope.criteriaList = criteria;
        $scope.genderList = [{id: 'f', text: 'Female'}, {id: 'm', text: 'Male'}];
        $scope.ageList = [{id: 'under 21', text: 'under 21'}, {id: 'over 21', text: 'over 21'}];
        $scope.campaign = campaign[0] || null;
        $scope.error = '';
        $scope.checkPaid = checkPaid;

        $scope.selectCriteriaOptions = [];
        $scope.selectedCriteria = [];
        $scope.categories = {};

        $scope.allHoursLimits = [
            {text: 'Select hours limit', value: 0},
            {text: '1 hour', value: 1},
            {text: '2 hours', value: 2},
            {text: '3 hours', value: 3},
            {text: '4 hours', value: 4},
            {text: '5 hours', value: 5},
            {text: '6 hours', value: 6},
            {text: '7 hours', value: 7},
            {text: '8 hours', value: 8},
            {text: '9 hours', value: 9},
            {text: '10 hours', value: 10},
            {text: '11 hours', value: 11},
            {text: '12 hours', value: 12},
            {text: '13 hours', value: 13},
            {text: '14 hours', value: 14},
            {text: '15 hours', value: 15},
            {text: '16 hours', value: 16},
            {text: '17 hours', value: 17},
            {text: '18 hours', value: 18},
            {text: '19 hours', value: 19},
            {text: '20 hours', value: 20},
            {text: '21 hours', value: 21},
            {text: '22 hours', value: 22},
            {text: '23 hours', value: 23},
            {text: '24 hours', value: 24}
        ];

        var index = 0;
        angular.forEach(companyCriteria, function (element) {
            if (!$scope.categories[element['category'].name]) {
                $scope.categories[element['category'].name] = {
                    label: element['category'].name,
                    items: []
                };

                $scope.selectedCriteria[index] = '0';

                $scope.categories[element['category'].name].items.push({
                    text: 'Select your criteria',
                    value: '0'
                });
                index++;
            }
            $scope.categories[element['category'].name].items.push({
                text: element['name'],
                value: element['name']
            });
        });

        var originalForm = angular.copy($scope.form);

        var joinItemIds = function (items) {
            var ids = [];
            angular.forEach(items, function (item) {
                ids.push(item.id);
            });
            return ids.join(",");
        };
        var createCampaign = function () {
            $scope.form.price = $scope.available * $scope.form.priceByUser;
            //$scope.form.available = $scope.form.opportunity.count;
            $scope.form.available = $scope.available;
            $scope.form.distance = $scope.form.opportunity.distance;
            $scope.form.userGroup = [];

            $scope.form.age = joinItemIds($scope.form.age);
            $scope.form.gender = joinItemIds($scope.form.gender);
            var submitData = angular.copy($scope.form);
            delete submitData.step;
            delete submitData.priceByUser;
            delete submitData.isSetHoursLimit;
            Campaign.save(submitData, function (data) {
                $scope.form.step = 3;
                $scope.campaign = data;
                bootbox.alert('Your card would be charged once it is accepted');
            }, function (err) {
                if (err) {
                    $scope.error = err.statusText;
                }
            });
        };

        $scope.save = function () {
            $scope.error = '';
            if ($scope.canSubmit()) {
                //var amount = $scope.form.opportunity.count * $scope.form.priceByUser * 2;
                //amount = 2;
                createCampaign();
                /*
                 if (amount === 0) {
                 createCampaign();
                 } else {
                 User.checkout({amount: amount},
                 createCampaign,
                 function (err) {
                 $scope.errors = err.data.errors.raw.message;
                 });
                 }*/
            }
        };

        $scope.closeAlert = function () {
            $scope.error = '';
        };

        $scope.isCampaignStarted = function () {
            return campaign.length === 1 && campaign[0].status !== 'reject';
        };

        $scope.isSetupPaymentInfo = function () {
            if (user.business.cardnumber && user.business.expMonth && user.business.expYear && user.business.CVC) {
                return true;
            } else {
                return false;
            }
        };

        $scope.canSubmit = function () {
            return $scope.form_campaign.$valid && $scope.form.age.length > 0 &&
                $scope.form.gender.length > 0 && $scope.form.criteria.length > 0 && !angular.equals($scope.form, originalForm) &&
                (!$scope.form.isSetHoursLimit || ($scope.form.isSetHoursLimit && $scope.form.hoursLimit != 0));
        };

        var init = function () {
            if (campaign.length > 0 && campaign[0].status !== 'reject') {
                $scope.error = 'Current campaign has not finished yet. You can not create a new campaign at the moment.';
            }

            $('.pie').easyPieChart({
                scaleColor: false,
                lineWidth: 20,
                lineCap: 'butt',
                barColor: '#ff6b6b',
                size: 150,
                animate: 500
            });
        };

        $scope.adjustHoursLimit = function() {
            if (!$scope.form.isSetHoursLimit) {
                $scope.form.hoursLimit = 0;
            }
        };

        $scope.selectCriteria = function ($index) {
            //console.log($index);
            if ($scope.selectedCriteria[$index] != 0 && $scope.form['criteria'].length < 3 &&
                _.findWhere($scope.form['criteria'], {text: $scope.selectedCriteria[$index]}) == undefined) {
                $scope.form['criteria'].push({
                    text: $scope.selectedCriteria[$index]
                });
            }
        };

        $scope.removeCriteria = function (index) {
            $scope.form['criteria'].splice(index, 1);
        };

        $scope.checkItem = function (field, isChecked, item) {
            if (isChecked) {
                $scope.form[field].push(item);
            } else {
				var temp = [];
				angular.forEach($scope.form[field], function(curItem) {
					if (curItem.id !== item.id) {
						temp.push(curItem);
					}
				});
				$scope.form[field] = temp;
                //$scope.form[field].splice($scope.form.criteria.indexOf(item), 1);
            }
			console.log(item);
        };

        $scope.$watchCollection('form.age', function(oldValue, newValue) {
            if (oldValue != newValue) {
                filterOpportunityUsers();
            }
        });

        $scope.$watchCollection('form.gender', function(oldValue, newValue) {
            if (oldValue != newValue) {
                filterOpportunityUsers();
            }
        });

        $scope.$watchCollection('form.criteria', function(oldValue, newValue) {
            if (oldValue != newValue) {
                filterOpportunityUsers();
            }
        });

        var opportunityUsersArr = (_.max(business.opportunity, function(opp) {
            return opp.distance;
        })).userGroup;
        $scope.available = opportunityUsersArr.length;

        var filterOpportunityUsers = function () {
            var filterObj = $scope.form;
            var addToArrFlag;
            $scope.available = 0;

            angular.forEach(opportunityUsersArr, function (user) {
                addToArrFlag = !(filterObj.age.length == 1 && filterObj.age[0].id !== user.age) &&
                    !(filterObj.gender.length == 1 && filterObj.gender[0].id !== user.gender);

                if (filterObj.criteria.length > 0 && addToArrFlag) {
                    addToArrFlag = addToArrFlag && filterObj.criteria.some(function(fObjCr) {
                        return user.criteria.some(function(userCr) {
                            return fObjCr.text === userCr.text;
                        });
                    })
                }

                if (addToArrFlag) {
                    $scope.available++;
                }
            })
        };

        $scope.filterReach = function (opportunityItem) {
            opportunityUsersArr = opportunityItem.userGroup;
            $scope.available = opportunityItem.userGroup.length;
            filterOpportunityUsers();
        };

        init();
    });
