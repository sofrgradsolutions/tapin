'use strict';

angular.module('tapinApp')
	.controller('SettingsCtrl', function ($scope, User, Auth, socket, user, $upload, Business, bootbox) {
		$scope.user = user;
		$scope.newEmail = '';
		if (!user.business.geo) {
			user.business.geo = ['', ''];
		}
		$scope.user.lat = user.business.geo[1];
		$scope.user.long = user.business.geo[0];
		$scope.businessCategories = ['Food', 'Drink', 'Health', 'Lifestyle', 'Shopping', 'Entertainment'];

		if (!angular.isArray($scope.user.business.description) || $scope.user.business.description.length == 0) {
			$scope.user.business.description = [''];
		}

		$scope.errors = {};
		$scope.update = function (form) {
			$scope.submitted = true;
			if (form.$valid) {
				delete $scope.user.__v;
				delete $scope.user._id;
				delete $scope.user.business.__v;
				delete $scope.user.business._id;
				var description = [];
				angular.forEach($scope.user.business.description, function(item) {
					if (item) {
						description.push(item);
					}
				});
				$scope.user.business.description = description;
				//$scope.user.business.businessCategory = ['Food', 'Drink', 'Health', 'Lifestyle', 'Shopping', 'Entertainment'];
				$scope.user.lat = $('#geoLat').val();
				$scope.user.long = $('#geoLong').val();

				$scope.user.businessCategory = $scope.user.business.businessCategory;
				User.update($scope.user, function (data) {
					bootbox.alert('User information successfully changed.');
				}, function (err) {

				});
			}
		};

		$scope.updateEmail = function () {
			User.changeEmailStep1({email: $scope.newEmail}, function(data){
				bootbox.alert('Change of email address request letter is successfully sent to your email.');
			}, function(err){
				bootbox.alert(err.data.message);
			});
		}

		$scope.toggleSelection = function toggleSelection(businessCategory) {
			var idx = $scope.user.business.businessCategory.indexOf(businessCategory);
			// is currently selected
			if (idx > -1) {
				$scope.user.business.businessCategory.splice(idx, 1);
			} else if ($scope.user.business.businessCategory.length < 3) {
				// is newly selected and selected before less then 3
				$scope.user.business.businessCategory.push(businessCategory);
			}
		};

		$scope.test = function (form) {
			var data = {
				category: 'Entertainment',
				distance: '100000',
				lat: 30,
				long: -118
			};
			/*
			User.getBusiness(data, function (data) {
				console.log(data);
			}, function (err) {
				console.log(err);
			});*/
			Business.trending();
		};

		var markers = [];

		var mapOptions = {
			center: new google.maps.LatLng($scope.user.business.geo[1], $scope.user.business.geo[0]),
			zoom: 8,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		var targetDiv = document.getElementById('map_canvas');

		$scope.map_canvas = new google.maps.Map(targetDiv,
				mapOptions);

		new google.maps.Marker({
			position: mapOptions.center,
			map: $scope.map_canvas
		});

		var input = document.getElementById('pac-input');
		$scope.map_canvas.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

		var searchBox = new google.maps.places.SearchBox(input);

		// Listen for the event fired when the user selects an item from the
		// pick list. Retrieve the matching places for that item.


		google.maps.event.addListener(searchBox, 'places_changed', function () {
			var places = searchBox.getPlaces();

			if (places.length == 0) {
				return;
			}
			for (var i = 0, marker; marker = markers[i]; i++) {
				marker.setMap(null);
			}

			// For each place, get the icon, place name, and location.
			markers = [];
			var bounds = new google.maps.LatLngBounds();
			for (var i = 0, place; place = places[i]; i++) {
				var image = {
					url: place.icon,
					size: new google.maps.Size(71, 71),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(17, 34),
					scaledSize: new google.maps.Size(25, 25)
				};

				// Create a marker for each place.
				var marker = new google.maps.Marker({
					map: $scope.map_canvas,
					icon: image,
					title: place.name,
					position: place.geometry.location
				});

				window.parent.document.getElementById('geoLat').value = place.geometry.location.lat();
				window.parent.document.getElementById('geoLong').value = place.geometry.location.lng();

				markers.push(marker);

				bounds.extend(place.geometry.location);
			}

			$scope.map_canvas.fitBounds(bounds);
		});

		// Bias the SearchBox results towards places that are within the bounds of the
		// current map's viewport.
		google.maps.event.addListener($scope.map_canvas, 'bounds_changed', function () {
			var bounds = $scope.map_canvas.getBounds();
			searchBox.setBounds(bounds);
		});

		$scope.onFileSelect = function($files) {
			var file = $files[0];
			$upload.upload({
				url: '/api/users/me/upload-image',
				file: file
			}).progress(function (evt) {
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				//console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
			}).success(function (data, status, headers, config) {
				//console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
				$scope.user.business.imageName = data.imageName;
			});
		};
	});
