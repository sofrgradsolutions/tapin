'use strict';

angular.module('tapinApp')
    .controller('SignupCtrl', function ($scope, Auth, $location, $window, User, $state) {
        var geocoder = new google.maps.Geocoder();
        $scope.user = {
            lat: 1,
            long: 1,
            step: 'confirm-email',
            isPaid: false,
            businessCategory: [],
            role: 'manager',
            criteria: []
        };

        $scope.errors = {};

        $scope.nextStep = function (form) {
            $scope.submitted = true;

            if (form.$valid) {

                var fullAddress = $scope.user.address + ', ' + $scope.user.zipCode;

                geocoder.geocode({'address': fullAddress}, function (results, status) {
                    if (results.length > 0) {
                        $scope.user.lat = results[0].geometry.location.lat();
                        $scope.user.long = results[0].geometry.location.lng();
                    }
                    Auth.createUser($scope.user)
                        .then(function () {
                            ///User.confirmEmail(function () {
                             // Account created, redirect to home
                            $state.go('confirm-email');
                            //});
                        })
                        .catch(function (err) {
                            err = err.data;
                            $scope.errors = {};

                            // Update validity of form fields that match the mongoose errors
                            angular.forEach(err.errors, function (error, field) {
                                form[field].$setValidity('mongoose', false);
                                $scope.errors[field] = error.message;
                            });
                        });
                });
            }
        };

        $scope.loginOauth = function (provider) {
            $window.location.href = '/auth/' + provider;
        };

    });
