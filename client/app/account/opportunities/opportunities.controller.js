'use strict';

angular.module('tapinApp')
	.controller('OpportunitiesCtrl', function ($scope, User, criteriaList, criteria, $location, bootbox) {
		$scope.criteriaList = criteriaList;
		$scope.data = {
			criteria: criteria
		};

    var inArray = function(arr, searchItem) {
      var isFind = false;
      angular.forEach(arr, function(item) {
        if (searchItem.text === item.text) {
          isFind = true;
        }
      });
      return isFind;
    };
		criteria = [];
		angular.forEach($scope.data.criteria, function(item) {
			if (inArray($scope.criteriaList, item)) {
				criteria.push(item);
			}
		});

		$scope.data.criteria = criteria;
		$scope.errors = {};

		$scope.update = function (form) {
			$scope.submitted = true;
			if (form.$valid) {
				User.updateCriteria($scope.data, function () {
          bootbox.alert('Opportunity Criteria successfully changed.');
					$location.path("/");
				}, function (err) {

				});
			}
		};

		$scope.checkCriteria = function (item){
			var selectedArr = [];
			var isFind = false;
			angular.forEach($scope.data.criteria, function(selectedItem) {
				if (selectedItem.text !== item.text) {
					selectedArr.push(selectedItem);
				} else {
					isFind = true;
				}
			});
			if (!isFind) {
				selectedArr.push(item);
			}
			$scope.data.criteria = selectedArr;
		};

		$scope.isCheckCriteria = function (item) {
			var isChecked = false;
			angular.forEach($scope.data.criteria, function(selectedItem) {
				if (selectedItem.text === item.text) {
					isChecked = true;
				}
			});
			return isChecked;
		};
	});
