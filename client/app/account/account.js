'use strict';

angular.module('tapinApp')
.config(function ($stateProvider) {
	$stateProvider
		.state('signup-login', {
			url: '/signup-login',
			templateUrl: 'app/account/signup-login/signup-login.html'
		})
		.state('login', {
			url: '/login',
			templateUrl: 'app/account/login/login.html',
			controller: 'LoginCtrl'
		})
		.state('recover-password', {
			url: '/recover-password',
			templateUrl: 'app/account/recover-password/recover-password.html',
			controller: 'RecoverPasswordCtrl',
			authenticate: false
		})
		.state('set-new-email', {
			url: '/set-new-email',
			templateUrl: 'app/account/set-new-email/set-new-email.html',
			controller: 'SetNewEmailCtrl',
			authenticate: false
		})
		.state('signup', {
			url: '/signup',
			templateUrl: 'app/account/signup/signup.html',
			controller: 'SignupCtrl',
			authenticate: false
		})
		.state('onboard', {
			url: '/onboard',
			templateUrl: 'app/account/onboard/onboard.html',
			controller: 'OnboardCtrl',
			resolve: {
				user: function (User) {
					return User.get().$promise;
				}
			}
		})
		.state('confirm-email', {
			url: '/confirm-email',
			templateUrl: 'app/account/confirm-email/confirm-email.html',
			controller: "ConfirmEmailCtrl",
			authenticate: false
		})
		.state('checkout', {
			url: '/checkout',
			templateUrl: 'app/account/checkout/checkout.html',
			controller: 'CheckoutCtrl',
			resolve: {
				user: function (User) {
					return User.get().$promise;
				}
			}
		})
		.state('settings', {
			url: '/settings',
			templateUrl: 'app/account/settings/settings.html',
			controller: 'SettingsCtrl',
			authenticate: true,
			resolve: {
				user: function (User) {
					return User.get().$promise;
				}
			}
		});
});
