'use strict';

angular.module('tapinApp')
  .controller('LoginCtrl', function ($scope, Auth, $location, $window, socket) {
    $scope.user = {};
    $scope.errors = {};

    $scope.login = function(form) {
      $scope.submitted = true;

      if(form.$valid) {
        Auth.login({
          email: $scope.user.email,
          password: $scope.user.password
        })
        .then( function(res) {
		  socket.reconnect();
          // Logged in, redirect to home
		  if (Auth.isAdmin()) {
			  //$location.path('/admin');
			  $window.location.href = '/admin';
			  console.log('admin');
		  } else {
			  $location.path('/');
		  }
        })
        .catch( function(err) {
          $scope.errors.other = err.message;
        });
      }
    };

    $scope.loginOauth = function(provider) {
      $window.location.href = '/auth/' + provider;
    };
  });
