'use strict';

angular.module('tapinApp')
	.controller('SetNewEmailCtrl', function ($scope, $location, User, Auth, bootbox) {
		$scope.errors = '';
		$scope.loaded = false;


		var token = $location.search().token || '';
		
		User.setNewEmail({token: token}, function (data) {
			bootbox.alert('Your email address has been successfully changed.');
			if (Auth.isLoggedIn()) {
				$location.path('/');
			} else {
				$location.path('/login');
			}
			$scope.loaded = true;
		}, function (err) {
			$scope.loaded = true;
			$scope.errors = err.data.message;
		});
	});
