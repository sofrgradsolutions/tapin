'use strict';

angular.module('tapinApp')
  .controller('CheckoutCtrl', function ($scope, Auth, $location, User, user) {
    $scope.user = user;
    $scope.errors = {};
	$scope.disableSubmitButton = false;

	Auth.setCurrentUser();
	
    $scope.checkout = function(form) {
      $scope.submitted = true;

      if(form.$valid) {
        $scope.user.businessCategory = $scope.user.business.businessCategory;
		$scope.user.businessType = $scope.user.business.businessType;
		delete $scope.user.__v;
		delete $scope.user._id;
		delete $scope.user.business.__v;
		delete $scope.user.business._id;
		$scope.disableSubmitButton = true;
		$scope.user.step = 'finish';
        User.update($scope.user, function() {
			Auth.setStep('finish');
			//$state.go('main');
			$location.path('/');
			/*
		  User.checkout(function(){
			Auth.setStep('finish');
			$location.path('/');
		  }, function(err) {
				var field = 'cardnumber';
				var transformFields = {
					'number': 'cardnumber',
					'exp_month': 'expMonth',
					'exp_year': 'expYear',
					'cvc': 'CVC'
				};
				if (angular.isDefined(transformFields[err.data.errors.raw.param])) {
					field = transformFields[err.data.errors.raw.param];
				}
				form[field].$setValidity('mongoose', false);
				$scope.errors[field] = err.data.errors.raw.message;
				$scope.disableSubmitButton = false;
		  });*/
        }, function(err) {
          err = err.data;
          $scope.errors = {};

          // Update validity of form fields that match the mongoose errors
          angular.forEach(err.errors, function(error, field) {
            form[field].$setValidity('mongoose', false);
            $scope.errors[field] = error.message;
          });
        });
      }
    };
  });
