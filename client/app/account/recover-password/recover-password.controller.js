'use strict';

angular.module('tapinApp')
	.controller('RecoverPasswordCtrl', function ($scope, Auth, $location, User) {
		$scope.user = {};
		$scope.errors = {};
		$scope.stage = $location.search().stage || 1;
		var recoverToken = $location.search().token || '';

		$scope.recover = function (form) {
			$scope.submitted = true;

			if (form.$valid) {
				Auth.recover({
					email: $scope.user.email
				})
				.then(function () {
					$scope.stage = 2;
				})
				.catch(function (err) {
					$scope.errors.other = err.message;
				});
			}
		};

		$scope.setNewPassword = function (form) {
			$scope.submitted = true;
			$scope.user.recoverToken = recoverToken;
			if (form.$valid && $scope.user.password === $scope.user.passwordConf) {
				User.setNewPassword($scope.user, function (data) {
					$scope.stage = 4;
				}, function (err) {
					$scope.errors.other = err.data.message;
					form['password'].$setValidity('mongoose', false);
				});
			}
		};

	});
