'use strict';

angular.module('tapinApp')
	.controller('OnboardCtrl', function ($scope, Auth, User, $location, user, $upload) {
		$scope.user = user;

		$scope.errors = {};
		$scope.businessCategories = ['Food', 'Drink', 'Health', 'Lifestyle', 'Shopping', 'Entertainment'];
		$scope.businessTypes = ['Store-front', 'Online'];
		
		if (!$scope.user.business) {
			$scope.user.business = {};
		}

		if (!angular.isArray($scope.user.business.description) || $scope.user.business.description.length == 0) {
			$scope.user.business.description = [''];
		}

		$scope.nextStep = function (form) {
			$scope.submitted = true;

			if (form.$valid && $scope.user.businessCategory.length > 0) {
				$scope.user.step = 'checkout';
				delete $scope.user.__v;
				delete $scope.user._id;
				delete $scope.user.business.__v;
				delete $scope.user.business._id;
				
				User.update($scope.user, function () {
					// Account created, redirect to home
					Auth.setStep('checkout');
					$location.path('/checkout');
				}, function (err) {
					err = err.data;
					$scope.errors = {};

					// Update validity of form fields that match the mongoose errors
					angular.forEach(err.errors, function (error, field) {
						form[field].$setValidity('mongoose', false);
						$scope.errors[field] = error.message;
					});
				});
			}
		};

		$scope.updateBusinessCategory = function (businessCategory, value) {
			if (angular.isUndefined($scope.user.businessCategory)) {
				$scope.user.businessCategory = [];
			}
			if (value) {
				$scope.user.businessCategory.push(businessCategory);
			} else {
				$scope.user.businessCategory.splice($scope.user.businessCategory.indexOf(businessCategory), 1);
			}
		};

		$scope.onFileSelect = function($files) {
			var file = $files[0];
			$upload.upload({
				url: '/api/users/me/upload-image',
				file: file
			}).progress(function (evt) {
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				//console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
			}).success(function (data, status, headers, config) {
				//console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
				$scope.user.business.imageName = data.imageName;
			});
		};
	});
