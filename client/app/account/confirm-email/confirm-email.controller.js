'use strict';

angular.module('tapinApp')
	.controller('ConfirmEmailCtrl', function ($scope, Auth, $location, User, sessionStore, $state, socket) {
		$scope.errors = {};
		$scope.stage = $location.search().stage || 1;
		var token = $location.search().token || '';

		var checkConfirmEmailToken = function () {

			User.checkConfirmEmail({token: token}, function (data) {
				sessionStore.put('token', data.token);
				sessionStore.put('step', data.step);
				socket.reconnect();
				$state.go('onboard');	
			}, function (err) {
				$scope.errors.other = err.data.message;
				$scope.stage = 3;
			});
		};

		if ($scope.stage == 2) {
			checkConfirmEmailToken();
		}

	});
