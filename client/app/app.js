'use strict';

angular.module('tapinApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'btford.socket-io',
    'ui.router',
    'ui.bootstrap',
    'ngTagsInput',
    'ngLodash',
    'angularFileUpload',
    'angular-bootbox',
    'ngDropdowns',
	'LocalStorageModule'
])
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $bootboxProvider) {
        $urlRouterProvider.otherwise('/');
        $locationProvider.html5Mode(true);
        $httpProvider.interceptors.push('authInterceptor');
        $bootboxProvider.setDefaults({locale: "en"});
    })

    .factory('authInterceptor', function ($rootScope, $q, sessionStore, $location, $window) {
        return {
            // Add authorization token to headers
            request: function (config) {
                config.headers = config.headers || {};
                if (sessionStore.get('token')) {
                    config.headers.Authorization = 'Bearer ' + sessionStore.get('token');
                }
                return config;
            },

            // Intercept 401s and redirect you to login
            responseError: function (response) {
                if (response.status === 401 || response.status === 404) {
                    // remove any stale tokens
                    sessionStore.remove('token');
					if (response.config.url !== '/auth/local') {
						$window.location = '/start.html';
					}
                    return $q.reject(response);
                } else {
                    return $q.reject(response);
                }
            }
        };
    })

    .run(function ($rootScope, $location, socket, Auth, User, $window, bootbox) {
        var isShowSocketMessage = false;
        // Redirect to login if route requires auth and you're not logged in
        $rootScope.$on('$stateChangeStart', function (event, next) {
            Auth.isLoggedInAsync(function (loggedIn) {
                if (next.authenticate && !loggedIn) {
                    $window.location = '/start.html';
                }
            });
            if (socket.socket != null) {
                //console.log('testestestest');
                socket.socket.on('campaign:accept', function (data) {
                    if (!isShowSocketMessage) {
                        bootbox.alert(data.message, function () {
                            isShowSocketMessage = false;
                        });
                        isShowSocketMessage = true;
                    }
                });

                socket.socket.on('campaign:reject', function (data) {
                    if (!isShowSocketMessage) {
                        bootbox.alert(data.message, function () {
                            isShowSocketMessage = false;
                        });
                        isShowSocketMessage = true;
                    }
                });
            }

            if (Auth.getStep() != null && Auth.getStep() !== 'finish' && next.url !== '/' + Auth.getStep()) {
                $location.path('/' + Auth.getStep());               
            }
        });
    });
