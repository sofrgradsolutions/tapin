'use strict';

angular.module('tapinApp')
    .controller('MainCtrl', function ($scope, $http, socket, opportunities, criteria, lodash, campaign, user, Business, $timeout) {
        $scope.awesomeThings = [];
        $scope.opportunities = opportunities;
        $scope.criteria = criteria;
        $scope.campaign = campaign[0] || null;
        $scope.currentOppt = {
            count: 0
        };
        $scope.data = {
            total: 0,
            campaignTotal: 0
        };
        $scope.user = user;


        var updateTotalCount = function () {
            $scope.data.total = 0;
            $scope.opportunities.forEach(function (item) {
                $scope.data.total += item.count;
            });
            if ($scope.data.total === 0) {
                $scope.data.total = 1;
            }
        };

        var updatePieChart = function () {
            var chart = $(".pie");
            chart.data('easy-pie-chart', null);
            chart.easyPieChart({
                scaleColor: false,
                lineWidth: 20,
                lineCap: 'butt',
                barColor: '#ff6b6b',
                size: 150,
                animate: 500
            });
			console.log('update chart');
        };
        /**
         * Initialize function
         */
        $scope.init = function () {
            // attach socket
            socket.socket.on('opportunity:update', function (data) {
                $scope.opportunities = data;
                $scope.opportunities.forEach(function (item) {
                    if (item.distance == $scope.currentOppt.distance) {
                        $scope.currentOppt = item;
                    }
                });
                updateTotalCount();
            });
            socket.socket.on('campaign:update', function (data) {
                $scope.campaign = data;
                $timeout(updatePieChart, 1000);
            });

            if ($scope.opportunities.length > 0) {
                $scope.currentOppt = $scope.opportunities[0];
            }
        };

        $scope.$on('$destroy', function () {
            socket.socket.removeAllListeners('opportunities:update');
            socket.socket.removeAllListeners('campaign:update');
        });

        $scope.updateOpportunity = function (oppt) {
            $scope.currentOppt = oppt;
        };

        $scope.showCriteria = function (value) {
            return lodash.pluck(value, 'text').join(', ');
        };

        $scope.isCampaignStarted = function () {
            return campaign.length == 1;
        };

        $scope.init();

        $scope.testClick = function () {
            //$scope.campaign.userScanned.push('445665677');
            //updatePieChart();
            /*
             angular.forEach($scope.opportunities, function(item) {
             item.count++;
             });
             updateTotalCount();*/
            Business.testShow({id: '54f706625e0dedeb2f9b6c3b'});

        };
        updateTotalCount();
    });
