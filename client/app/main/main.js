'use strict';

angular.module('tapinApp')
	.config(function ($stateProvider) {
		var criteriaList = [
			{text: 'Vegan'}, {text: 'Mountain'}, {text: 'Clothingsales'}, 
			{text: 'Clubs'}, {text: 'Rockmusic'},
			{text: 'Bars'}, {text: 'Happyhour'}, {text: 'Male'}, 
			{text: 'Wine'}, {text: 'Beach'}, {text: 'Dancing'},
			{text: 'Health'}, {text: 'Female'}, {text: 'Green Juice'}
		];
		$stateProvider
			.state('main', {
				url: '/',
				templateUrl: 'app/main/main.html',
				controller: 'MainCtrl',
				resolve: {
					opportunities: function (User) {
						return User.getOpportunities().$promise;
					},
					criteria: function (User) {
						return User.getCriteria().$promise;
					},
					campaign: function (User) {
						return User.getCampaign().$promise;
					},
                    user: function (User) {
                        return User.get().$promise;
                    }
				},
				authenticate: true
			});
	});
