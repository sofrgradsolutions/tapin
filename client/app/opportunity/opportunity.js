'use strict';

angular.module('tapinApp')
  .config(function ($stateProvider) {
    var criteriaList = [
      {text: 'Vegan'}, {text: 'Mountain'}, {text: 'Clothingsales'},
      {text: 'Clubs'}, {text: 'Rockmusic'},
      {text: 'Bars'}, {text: 'Happyhour'}, {text: 'Male'},
      {text: 'Wine'}, {text: 'Beach'}, {text: 'Dancing'},
      {text: 'Health'}, {text: 'Female'}, {text: 'Green Juice'}
    ];

    var opportunityCriteriaList = [
      {text: 'One'}, {text: 'Two'}, {text: 'Three'},
      {text: 'Four'}, {text: 'Five'}
    ];
/*
    $stateProvider
      .state('opportunity', {
        url: '/opportunity',
        templateUrl: 'app/opportunity/opportunity.html',
        controller: 'OpportunityCtrl',
        resolve: {
          criteria: function (User) {
            return criteriaList;//User.getCriteria().$promise;
          },
          opportunities: function (User) {
            return opportunityCriteriaList; //User.getOpportunities().$promise;
          },
          campaign: function (User) {
            return User.getCampaign().$promise;
          }
        }
      });
*/
  });
