'use strict';

angular.module('tapinApp')
  .controller('OpportunityCtrl', function ($scope, opportunityCriteria, User) {
    $scope.form = {
      price: 18,
      criteria: [],
      limit: 30
      //step: 1
    };
    $scope.opportunities = opportunities;
    $scope.criteriaList = criteria;
    $scope.opportunityCriteriaList = opportunityCriteria;
    $scope.campaign = campaign[0] || null;
    $scope.error = '';

    var originalForm = angular.copy($scope.form);

    $scope.save = function () {
      $scope.error = '';
      if ($scope.canSubmit()) {
        $scope.form.available = $scope.form.opportunity.count;
        $scope.form.distance = $scope.form.opportunity.distance;
        $scope.form.userGroup = [];
        var submitData = angular.copy($scope.form);
        delete submitData.step;
        Campaign.save(submitData, function (data) {
          $scope.form.step = 3;
          $scope.campaign = data;
          $scope.qr_img = "https://api.qrserver.com/v1/create-qr-code/?size=150*150&data=" + data._id;
        }, function (err) {
          if (err) {
            $scope.error = err.statusText;
          }
        });
      }
    };

    $scope.closeAlert = function () {
      $scope.error = '';
    };

    $scope.isOpportunityStarted = function () {
      return opportunity.length == 1;
    };

    $scope.canSubmit = function (form) {
      return $scope.form_campaign.$valid && $scope.form.criteria.length > 0 && !angular.equals($scope.form, originalForm);
    };

    var init = function () {
      if (campaign.length != 0) {
        $scope.error = 'Current campaign has not finished yet. You can not create a new campaign at the moment.';
      }
    };

    $scope.checkCriteria = function (isChecked, item){
      if (isChecked) {
        $scope.form.criteria.push(item);
      } else {
        $scope.form.criteria.splice($scope.form.criteria.indexOf(item), 1);
      }
    };

    init();
  });
